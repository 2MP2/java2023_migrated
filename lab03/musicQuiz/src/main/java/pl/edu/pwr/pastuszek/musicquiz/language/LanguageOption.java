package pl.edu.pwr.pastuszek.musicquiz.language;

public enum LanguageOption {
    PL, EN;

    public String getCountry(){
        switch (this){
            case EN -> {
                return "EN";
            }
            case PL -> {
                return "PL";
            }
            default -> {
                return null;
            }
        }
    }

    public String getLanguage(){
        switch (this){
            case EN -> {
                return "en";
            }
            case PL -> {
                return "pl";
            }
            default -> {
                return null;
            }
        }
    }
}
