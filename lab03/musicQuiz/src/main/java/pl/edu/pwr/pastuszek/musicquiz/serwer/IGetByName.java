package pl.edu.pwr.pastuszek.musicquiz.serwer;

import java.io.IOException;

public interface IGetByName<T> {
    T getByName(String name) throws IOException;
}
