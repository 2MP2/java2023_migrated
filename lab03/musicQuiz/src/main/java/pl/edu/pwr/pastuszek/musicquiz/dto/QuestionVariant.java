package pl.edu.pwr.pastuszek.musicquiz.dto;

public record QuestionVariant(String type, String name, String ip) {

    @Override
    public String toString() {
        return name;
    }
}
