package pl.edu.pwr.pastuszek.musicquiz.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recording{
    @JsonProperty
    private long length;
    @JsonProperty
    private String id;
    @JsonProperty(value = "first-release-date")
    private Date firstReleaseDate;
    @JsonProperty
    private String title;
    @JsonProperty(value = "artist-credit")
    private List<ArtistCredits> artistCredits;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ArtistCredits {
        @JsonProperty
        private Artist artist;
        @JsonProperty(value = "joinphrase")
        private String joinPhrase;
    }


}
