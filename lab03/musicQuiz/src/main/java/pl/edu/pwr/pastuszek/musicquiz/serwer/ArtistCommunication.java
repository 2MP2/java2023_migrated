package pl.edu.pwr.pastuszek.musicquiz.serwer;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pl.edu.pwr.pastuszek.musicquiz.dto.Artist;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class ArtistCommunication extends AbstractCommunication<Artist>  {
    public ArtistCommunication(OkHttpClient client, ObjectMapper objectMapper) throws URISyntaxException {
        super(client, objectMapper,new URI("https://musicbrainz.org/ws/2/"),
                Headers.of("User-Agent","test-app","Accept","application/json"));
    }
    public ArtistCommunication(OkHttpClient client, ObjectMapper objectMapper, Headers headers) throws URISyntaxException {
        super(client, objectMapper, new URI("https://musicbrainz.org/ws/2/"),
                Headers.of("User-Agent","test-app","Accept","application/json")
                        .newBuilder().addAll(headers).build());
    }


    @Override
    public Artist getById(String id) throws IOException {
        //template: https://musicbrainz.org/ws/2/artist/id
            URL url = super.uri.resolve("artist/" + id ).toURL();
            return getArtist(url);

    }

    @Override
    public Artist getByName(String name) throws IOException {
        //template: https://musicbrainz.org/ws/2/artist/?query=artist:name&limit=1
            URL url = super.uri.resolve("artist/?query=artist:" + name + "&limit=1").toURL();
            return getArtist(url);

    }

    private Artist getArtist(URL url) throws IOException{
        Request request = new Request.Builder().url(url).headers(super.headers).build();

        try (Response response = client.newCall(request).execute()) {
            return objectMapper.readValue(response.body().string(), Artist.class);
        }
    }
}
