package pl.edu.pwr.pastuszek.musicquiz.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Artist {
    @JsonProperty
    private String id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String type;
    @JsonProperty
    private String country;
    @JsonProperty(value = "begin-area")
    private BeginArea beginArea;
    @JsonProperty(value = "life-span")
    private LifeSpan lifeSpan;
    @JsonProperty(value = "end-area")
    private EndArea endArea;
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
     public static class BeginArea{
        @JsonProperty
        private String id;
        @JsonProperty
        private String name;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LifeSpan{
        @JsonProperty
        private Date begin;
        @JsonProperty
        private boolean ended;
        @JsonProperty
        private Date end;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class EndArea{
        @JsonProperty
        private String id;
        @JsonProperty
        private String name;
    }

}