package pl.edu.pwr.pastuszek.musicquiz;

import pl.edu.pwr.pastuszek.musicquiz.dto.QuestionVariant;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class QuestionReader{

    public List<QuestionVariant> getQuestions(Path path) throws IOException, IndexOutOfBoundsException {
        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
        List<QuestionVariant> questionVariantList = new ArrayList<>();
        for (int i = 0; i < lines.size(); i += 3) {
            QuestionVariant questionVariant = new QuestionVariant(
                    lines.get(i),
                    lines.get(i + 1),
                    lines.get(i + 2));

            questionVariantList.add(questionVariant);
        }
        return questionVariantList;
    }
}

