package pl.edu.pwr.pastuszek.musicquiz.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import okhttp3.OkHttpClient;
import pl.edu.pwr.pastuszek.musicquiz.QuestionReader;
import pl.edu.pwr.pastuszek.musicquiz.dto.Artist;
import pl.edu.pwr.pastuszek.musicquiz.dto.QuestionVariant;
import pl.edu.pwr.pastuszek.musicquiz.dto.Recording;
import pl.edu.pwr.pastuszek.musicquiz.language.LanguageMaster;
import pl.edu.pwr.pastuszek.musicquiz.language.LanguageOption;
import pl.edu.pwr.pastuszek.musicquiz.serwer.ArtistCommunication;
import pl.edu.pwr.pastuszek.musicquiz.serwer.RecordingCommunication;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ResourceBundle;


public class StartController {
    @FXML private Label titleLabel;
    @FXML private Label questionLabel1,questionLabel2,questionLabel3,questionLabel4;
    @FXML private Label answerLabel1,answerLabel2,answerLabel3,answerLabel4;
    @FXML private Label tfLabel1,tfLabel2,tfLabel3,tfLabel4;
    @FXML private ComboBox <QuestionVariant> questionComboBox1,questionComboBox2,questionComboBox3,questionComboBox4;
    @FXML private TextField answerTextField1,answerTextField2,answerTextField3,answerTextField4;
    @FXML private Button answerButton1,answerButton2,answerButton3,answerButton4;
    @FXML private ComboBox<LanguageOption> languageComboBox;
    @FXML private GridPane gridPane1,gridPane2,gridPane3,gridPane4;
    @FXML private AnchorPane anchorPane;

    private final LanguageMaster lg = LanguageMaster.getLanguageMaster();
    private final OkHttpClient oc = new OkHttpClient();
    private final ObjectMapper om = new ObjectMapper();
    private final ArtistCommunication artistCommunication;
    private final RecordingCommunication recordingCommunication;

    public StartController(){
        try {
            artistCommunication = new ArtistCommunication(oc,om);
            recordingCommunication = new RecordingCommunication(oc,om);
        } catch (URISyntaxException e) {
            throw new RuntimeException("XXX");
        }
    }


    public void initialize() {
        setStyle();

        languageComboBox.getItems().addAll(LanguageOption.PL,LanguageOption.EN);
        languageComboBox.setOnAction(actionEvent ->{
            setLanguage(languageComboBox.getValue().getLanguage(),languageComboBox.getValue().getCountry());
        } );
        languageComboBox.setValue(LanguageOption.PL);
        setLanguage(languageComboBox.getValue().getLanguage(),languageComboBox.getValue().getCountry());

        try {
            loadQuestionsVariants();
        }catch (Exception e){
            System.out.println(lg.getResourceBundle().getString("errorLoadQuestion"));
        }
    }

    private void setLanguage(String language, String country){
        String bundlePath = "pl.edu.pwr.pastuszek.musicquiz.Bundle";
        lg.setResourceBundle(bundlePath,language,country);
        ResourceBundle resourceBundle = lg.getResourceBundle();
        if(resourceBundle!= null){
            titleLabel.setText(resourceBundle.getString("titleLabel"));

            Object[] objects1 = {lg.getResourceBundle().getString("dateFormat")};
            Object[] objects2 = {lg.getResourceBundle().getString("yes"), lg.getResourceBundle().getString("no")};

            questionLabel1.setText(lg.getFormatString("question1",objects1));
            questionLabel2.setText(lg.getFormatString("question2",objects2));
            questionLabel3.setText(lg.getFormatString("question3",objects1));
            questionLabel4.setText(resourceBundle.getString("question4"));

            answerLabel1.setText(resourceBundle.getString("answerLabel"));
            answerLabel2.setText(resourceBundle.getString("answerLabel"));
            answerLabel3.setText(resourceBundle.getString("answerLabel"));
            answerLabel4.setText(resourceBundle.getString("answerLabel"));

            answerButton1.setText(resourceBundle.getString("answerButton"));
            answerButton2.setText(resourceBundle.getString("answerButton"));
            answerButton3.setText(resourceBundle.getString("answerButton"));
            answerButton4.setText(resourceBundle.getString("answerButton"));
        }
    }

    void setStyle(){
        anchorPane.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        gridPane1.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        gridPane2.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        gridPane3.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        gridPane4.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        answerButton1.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        answerButton2.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        answerButton3.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        answerButton4.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        questionComboBox1.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        questionComboBox2.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        questionComboBox3.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        questionComboBox4.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
        languageComboBox.setStyle("-fx-background-color: linear-gradient(from 0% 0% to 100% 100%, #E3F6CE, #B7C7FF, #D6A2E8); -fx-background-radius: 10; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.3), 10, 0, 0, 0);");
    }

    private void loadQuestionsVariants() throws IOException {
        QuestionReader questionReader = new QuestionReader();
        Path path = Paths.get("src/main/resources/pl/edu/pwr/pastuszek/musicquiz/questions_variants.txt");
        for (var q: questionReader.getQuestions(path)) {
           if(q.type().equals("AR")){
               questionComboBox1.getItems().add(q);
               questionComboBox2.getItems().add(q);
           }else if(q.type().equals("RC")){
               questionComboBox3.getItems().add(q);
               questionComboBox4.getItems().add(q);
           }else{
               System.out.println(lg.getResourceBundle().getString("errorQuestion"));
           }
        }
    }

    @FXML
    private void checkQuestion1() {
        Artist artist;
        try {
            artist = artistCommunication.getById(questionComboBox1.getValue().ip());
        }catch (IOException e){
            tfLabel1.setText(lg.getResourceBundle().getString("error"));
            return;
        }catch (NullPointerException e){
            tfLabel1.setText(lg.getResourceBundle().getString("chooseQuestion"));
            return;
        }

        try {
            Object[] arguments;

            if(lg.compareDate(artist.getLifeSpan().getBegin(), answerTextField1.getText()) == 0){
                arguments = new Object[] { lg.getResourceBundle().getString("trueAnswer"),
                        questionComboBox1.getValue().name(),
                        artist.getLifeSpan().getBegin()};
            }else{
                arguments = new Object[] { lg.getResourceBundle().getString("falseAnswer"),
                        questionComboBox1.getValue().name(),
                        artist.getLifeSpan().getBegin()};
            }
            tfLabel1.setText(lg.getFormatString("answer1", arguments));
        }catch (ParseException e){
            tfLabel1.setText(lg.getResourceBundle().getString("wrongFormatDate"));
        }

    }

    @FXML
    private void checkQuestion2() {
        try {
            Artist artist = artistCommunication.getById(questionComboBox2.getValue().ip());
            Object[] arguments;
            if(lg.weakCompareString(artist.getLifeSpan().isEnded() ? lg.getResourceBundle().getString("no")
                    : lg.getResourceBundle().getString("yes"), answerTextField2.getText()) == 0){
                arguments = new Object[] {lg.getResourceBundle().getString("trueAnswer")};
            }else{
                arguments = new Object[] {lg.getResourceBundle().getString("falseAnswer")};
            }
            tfLabel2.setText(lg.getFormatString("answer2", arguments));
        }catch (IOException e){
            tfLabel2.setText(lg.getResourceBundle().getString("error"));
        }catch (NullPointerException e){
            tfLabel2.setText(lg.getResourceBundle().getString("chooseQuestion"));
        }

    }

    @FXML
    private void checkQuestion3() throws IOException, ParseException {
        Recording recording;
        try {
            recording = recordingCommunication.getById(questionComboBox3.getValue().ip());
        }catch (IOException e){
            tfLabel3.setText(lg.getResourceBundle().getString("error"));
            return;
        }catch (NullPointerException e){
            tfLabel3.setText(lg.getResourceBundle().getString("chooseQuestion"));

            return;
        }

        try {
            Object[] arguments;
            if(lg.compareDate(recording.getFirstReleaseDate(),answerTextField3.getText()) == 0){
                arguments = new Object[] { lg.getResourceBundle().getString("trueAnswer"),
                        questionComboBox3.getValue().name(),
                        recording.getFirstReleaseDate()};
            }else{
                arguments = new Object[] { lg.getResourceBundle().getString("falseAnswer"),
                        questionComboBox3.getValue().name(),
                        recording.getFirstReleaseDate()};
            }
            tfLabel3.setText(lg.getFormatString("answer3", arguments));
        }catch (ParseException e){
            tfLabel3.setText(lg.getResourceBundle().getString("wrongFormatDate"));
        }

    }

    @FXML
    private void checkQuestion4() throws IOException {
        try {
            Recording recording = recordingCommunication.getById(questionComboBox4.getValue().ip());
            boolean flag = false;
            String artist = "-";
            for (var ac : recording.getArtistCredits()){
                if(lg.weakCompareString(ac.getArtist().getName(),answerTextField4.getText()) == 0){
                    flag = true;
                }
                artist = ac.getArtist().getName();
            }

            Object[] arguments;
            if(flag){
                arguments = new Object[]{lg.getResourceBundle().getString("trueAnswer"),artist};
            }else{
                arguments = new Object[]{lg.getResourceBundle().getString("falseAnswer"),artist};
            }
            tfLabel4.setText(lg.getFormatString("answer4", arguments));
        }catch (IOException e){
            tfLabel4.setText(lg.getResourceBundle().getString("error"));
        }catch (NullPointerException e){
            tfLabel4.setText(lg.getResourceBundle().getString("chooseQuestion"));
        }


    }



}