package pl.edu.pwr.pastuszek.musicquiz.serwer;

import java.io.IOException;

public interface IGetById<T>{

    T getById(String id) throws IOException;
}
