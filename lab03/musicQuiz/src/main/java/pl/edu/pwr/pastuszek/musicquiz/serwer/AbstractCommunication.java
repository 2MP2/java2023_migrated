package pl.edu.pwr.pastuszek.musicquiz.serwer;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Headers;
import okhttp3.OkHttpClient;

import java.net.URI;

public abstract class AbstractCommunication<T> implements IGetById<T> , IGetByName<T>{
    protected final OkHttpClient client;
    protected final ObjectMapper objectMapper;
    protected Headers headers;
    protected URI uri;

    public AbstractCommunication(OkHttpClient client, ObjectMapper objectMapper, URI uri, Headers headers) {
        this.client = client;
        this.objectMapper = objectMapper;
        this.headers = headers;
        this.uri = uri;
    }
}
