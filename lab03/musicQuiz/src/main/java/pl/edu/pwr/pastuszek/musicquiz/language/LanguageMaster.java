package pl.edu.pwr.pastuszek.musicquiz.language;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class LanguageMaster {
    private static final LanguageMaster languageMaster = new LanguageMaster();
    private ResourceBundle resourceBundle;

    private LanguageMaster() {
        setResourceBundle("pl.edu.pwr.pastuszek.musicquiz.Bundle","pl","PL");
    }

    public static LanguageMaster getLanguageMaster() {
        return languageMaster;
    }

    public void setResourceBundle(String name, String language, String country){
            Locale locale = new Locale(language,country);
            this.resourceBundle = ResourceBundle.getBundle(name, locale);
    }
    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    /**
     *
     * @param date1 can be any date
     * @param date2 the date must be entered in the current date format
     * @return compared to dates without hours,minutes,seconds and milliseconds
     * @throws ParseException
     */
    public int compareDate(Date date1, String date2) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(resourceBundle.getString("dateFormat"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date1 = calendar.getTime();

        return compareDate(date1,dateFormat.parse(date2));
    }
    public int compareDate(Date date1, Date date2){
        return date1.compareTo(date2);
    }

    public int weakCompareString(String s1, String s2){
        return s1.toUpperCase().compareTo(s2.toUpperCase());
    }

    public String getFormatString(String template, Object[] arguments){
        MessageFormat mf = new MessageFormat("");
        mf.setLocale(resourceBundle.getLocale());
        mf.applyPattern(resourceBundle.getString(template));
        return mf.format(arguments);
    }
}
