package pl.edu.pwr.pastuszek.musicquiz.serwer;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pl.edu.pwr.pastuszek.musicquiz.dto.Recording;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class RecordingCommunication extends AbstractCommunication<Recording>{
    public RecordingCommunication(OkHttpClient client, ObjectMapper objectMapper) throws URISyntaxException {
        super(client, objectMapper,new URI("https://musicbrainz.org/ws/2/"),
                Headers.of("User-Agent","test-app","Accept","application/json"));
    }
    public RecordingCommunication(OkHttpClient client, ObjectMapper objectMapper, Headers headers) throws URISyntaxException {
        super(client, objectMapper, new URI("https://musicbrainz.org/ws/2/"),
                Headers.of("User-Agent","test-app","Accept","application/json")
                        .newBuilder().addAll(headers).build());
    }

    @Override
    public Recording getById(String id) throws IOException {
        //template: https://musicbrainz.org/ws/2/recording/id?inc=artists

        URL url = super.uri.resolve("recording/" + id + "?inc=artists").toURL();
        return getRecording(url);
    }

    @Override
    public Recording getByName(String name) throws IOException { //Dont use it
        //template: https://musicbrainz.org/ws/2/recording/?query=recording:name&limit=1

        URL url = super.uri.resolve("recording/?query=recording:" + name + "&limit=1").toURL();
        return getRecording(url);
    }

    private Recording getRecording(URL url) throws IOException {
        Request request = new Request.Builder().url(url).headers(super.headers).build();
        try (Response response = client.newCall(request).execute()) {
            return objectMapper.readValue(response.body().string(), Recording.class);
        }
    }
}
