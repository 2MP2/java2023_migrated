module pl.edu.pwr.pastuszek.musicquiz {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires okhttp3;

    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires static lombok;

    opens pl.edu.pwr.pastuszek.musicquiz to javafx.fxml;
    exports pl.edu.pwr.pastuszek.musicquiz;
    exports pl.edu.pwr.pastuszek.musicquiz.controllers;
    opens pl.edu.pwr.pastuszek.musicquiz.controllers to javafx.fxml;
    exports pl.edu.pwr.pastuszek.musicquiz.serwer;
    opens pl.edu.pwr.pastuszek.musicquiz.serwer to javafx.fxml;
    exports pl.edu.pwr.pastuszek.musicquiz.dto to com.fasterxml.jackson.databind;
    exports pl.edu.pwr.pastuszek.musicquiz.language;
    opens pl.edu.pwr.pastuszek.musicquiz.language to javafx.fxml;

}