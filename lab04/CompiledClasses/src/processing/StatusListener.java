package processing;

import java.util.Collection;

public interface StatusListener {

	void statusChanged(Status s);
	Collection<String> getStatuses();
}
