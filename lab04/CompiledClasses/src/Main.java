import example.MyProcessorC;
import example.MyStatusListener;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        MyProcessorC myProcessorA = new MyProcessorC();
        MyStatusListener myStatusListener = new MyStatusListener();


        myProcessorA.submitTask("1 1",myStatusListener);
        while (!myStatusListener.getStatuses().isEmpty()){
            System.out.println(myStatusListener.getStatuses());
        }

        Thread.sleep(100);
        System.out.println(myProcessorA.getResult());

        myProcessorA.submitTask("bbb",myStatusListener);

        Thread.sleep(100);
        while (!myStatusListener.getStatuses().isEmpty()){
            System.out.println(myStatusListener.getStatuses());
        }


        Thread.sleep(5000);
        System.out.println(myProcessorA.getResult());
    }
}
