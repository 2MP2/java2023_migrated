package example;

import processing.Status;
import processing.StatusListener;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MyStatusListener implements StatusListener {
    public final ConcurrentMap<Integer,Status> concurrentHashMap;

    public MyStatusListener(){
        concurrentHashMap = new ConcurrentHashMap<>();
    }
    @Override
    public synchronized void statusChanged(Status s) {
        if(s.getProgress()>=100){
            concurrentHashMap.remove(s.getTaskId());
            return;
        }
        concurrentHashMap.put(s.getTaskId(),s);

    }

    @Override
    public synchronized List<String> getStatuses() {
        return concurrentHashMap.values().stream().map(Status::toString).toList();
    }
}
