package example;

import processing.Processor;
import processing.Status;
import processing.StatusListener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyProcessorC implements Processor {
    private static int taskId=0;
    private boolean transfer = true;
    private String result = null;
    @Override
    public boolean submitTask(String task, StatusListener sl) {
        taskId++;
        ExecutorService executorReceive = Executors.newSingleThreadExecutor();
        ExecutorService executorSend = Executors.newSingleThreadExecutor();

        executorSend.submit(() -> {
            for(int i=1;i<=100;i++){
                try {
                    sl.statusChanged(new Status(taskId,i));
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            transfer = false;
            synchronized (this){
                notifyAll();
            }

        });

        executorReceive.submit(() -> {
            while (transfer) {
                try {
                    synchronized (this){
                        wait();
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            result = count(task);
            executorSend.shutdown();
            executorReceive.shutdown();
        });

        return true;
    }
    @Override
    public String getInfo() {
        return "Add numbers";
    }

    @Override
    public String getResult() {
        return result;
    }

    private String count(String equation){
        String[] numbers = equation.split( " ");
        int sum = 0;
        for (String number : numbers) sum += Integer.parseInt(number);

        return String.valueOf(sum);
    }
}
