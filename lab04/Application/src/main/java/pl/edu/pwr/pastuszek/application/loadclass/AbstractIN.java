package pl.edu.pwr.pastuszek.application.loadclass;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractIN {
    protected final Object object;
    protected final Class<?> implementInterface;

    protected AbstractIN(Class<?> c, String i) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
            this.object = c.getConstructor().newInstance();
            this.implementInterface = setImplementInterface(c,i);
    }

    private Class<?> setImplementInterface(Class<?> c,String name){
        Class<?>[] interfaces = c.getInterfaces();
        for(Class<?> i: interfaces){
            if(i.getName().contains(name)){
                return i;
            }
        }
        return null;
    }

    public Object getThisObject() {
        return this.object;
    }

    public Class<?> getImplementInterface() {
        return implementInterface;
    }
}
