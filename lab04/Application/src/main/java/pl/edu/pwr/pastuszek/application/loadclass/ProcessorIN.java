package pl.edu.pwr.pastuszek.application.loadclass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ProcessorIN extends AbstractIN{
    public static final String INTERFACE = "Processor";

    public ProcessorIN(Class<?> c) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IllegalArgumentException {
        super(c, INTERFACE);
    }


    public boolean submitTask(String task, ListenerIN sl) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = object.getClass().getMethod("submitTask",task.getClass(),sl.implementInterface);
        return (boolean) method.invoke(object,task,sl.getThisObject());
    }
    public String getInfo() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = object.getClass().getMethod("getInfo");
        return (String) method.invoke(object);

    }
    public String getResult() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = object.getClass().getMethod("getResult");
        return (String) method.invoke(object);
    }

    @Override
    public String toString() {
        try {
            return getInfo();
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            return "ERROR";
        }
    }
}
