package pl.edu.pwr.pastuszek.application.loadclass;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomClassLoader extends java.lang.ClassLoader {
    private final Path path;

    public CustomClassLoader(Path path) {
        if (!Files.isDirectory(path)) throw new IllegalArgumentException("Path must be a directory");
        this.path = path;
    }

    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] classBytes = loadClassBytes(name);
        return defineClass(name, classBytes, 0, classBytes.length);
    }
    private byte[] loadClassBytes(String className) throws ClassNotFoundException {
        try {
            Path classfile = Paths.get(path + FileSystems.getDefault().getSeparator()
                    + className.replace(".", FileSystems.getDefault().getSeparator()) + ".class");

            return Files.readAllBytes(classfile);
        } catch (IOException e) {
            throw new ClassNotFoundException("Error in defining " + className + " in " + path,e);
        }
    }

    public List<Class<?>> loadClassTree(){

        try {
            List<Class<?>> classList = new ArrayList<>();
            Files.walkFileTree(this.path,new SimpleFileVisitor<Path>(){
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    String relativizedPath = path.relativize(file).toString();
                    try {
                        relativizedPath = relativizedPath.replace(".class","");
                        relativizedPath = relativizedPath.replace(FileSystems.getDefault().getSeparator(),".");
                        Class<?> c = loadClass(relativizedPath);
                        classList.add(c);
                    } catch (ClassNotFoundException | NoClassDefFoundError e) {
                        return FileVisitResult.CONTINUE;
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
            return classList;
        }catch (IOException | SecurityException e){
            return Collections.emptyList();
        }
    }

}
