package pl.edu.pwr.pastuszek.application;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import pl.edu.pwr.pastuszek.application.loadclass.CustomClassLoader;
import pl.edu.pwr.pastuszek.application.loadclass.ListenerIN;
import pl.edu.pwr.pastuszek.application.loadclass.ProcessorIN;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class StartController {
    @FXML private TextField pathTextField;
    @FXML private TextField inputTextField;
    @FXML private ListView<String> executingListView;
    @FXML private ListView<ProcessorIN> chooseTaskListView;
    @FXML private AnchorPane anchorPane;
    @FXML private TextArea resultTextArea;
    private Path currentPath;
    private final List<Class<?>> classList;
    private final List<ProcessorIN> processorINList;
    private final List<ListenerIN> listenerINList;
    private CustomClassLoader customClassLoader;
    public StartController(){
        classList = new ArrayList<>();
        processorINList = new ArrayList<>();
        listenerINList = new ArrayList<>();
        currentPath = null;
    }

    @FXML
    private void runTask() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        if(chooseTaskListView.getSelectionModel().isEmpty() || inputTextField.getText().isBlank()){
            return;
        }
        boolean takeTask = chooseTaskListView.getSelectionModel().getSelectedItem().submitTask(inputTextField.getText(),listenerINList.get(0));

        if(takeTask){
            ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            scheduledExecutorService.scheduleAtFixedRate(()-> Platform.runLater(()->{
                try {
                    Collection<String> collection = listenerINList.get(0).getStatus();
                    if (listenerINList.get(0).getStatus().isEmpty()) {
                        executingListView.getItems().clear();
                        resultTextArea.appendText( chooseTaskListView.getSelectionModel().getSelectedItem().getResult() + "\n");
                        scheduledExecutorService.shutdown();
                    }

                    executingListView.getItems().clear();
                    executingListView.getItems().addAll(collection);
                }catch (Exception e){

                }
            }),0,40, TimeUnit.MILLISECONDS);
        }
    }

    @FXML
    private void findDirectory() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        this.currentPath = browseDirectory(pathTextField);
        refresh();
    }

    @FXML
    private void refresh() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException{
        clear();
        if(currentPath!=null){
            customClassLoader = new CustomClassLoader(this.currentPath);
            this.classList.addAll(customClassLoader.loadClassTree());

            for (Class<?> c:classList) {
                Class<?>[] interfaces = c.getInterfaces();
                for (Class<?> itf: interfaces){
                    if(itf.getName().contains(ProcessorIN.INTERFACE)){
                        processorINList.add(new ProcessorIN(c));
                    } else if (itf.getName().contains(ListenerIN.INTERFACE)) {
                        listenerINList.add(new ListenerIN(c));
                    }
                }
            }
            chooseTaskListView.getItems().addAll(processorINList);
        }

    }
    @FXML
    private void clear(){
        chooseTaskListView.getItems().clear();
        listenerINList.clear();
        processorINList.clear();
        classList.clear();
        customClassLoader = null;
        System.gc();
    }

    private Path browseDirectory(TextField textField){
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        File file = directoryChooser.showDialog(stage);

        if(file !=null && file.isDirectory()){
            textField.setText(file.getAbsolutePath());
            return file.toPath();
        }else
            return null;
    }

}