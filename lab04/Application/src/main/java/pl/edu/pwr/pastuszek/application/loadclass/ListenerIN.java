package pl.edu.pwr.pastuszek.application.loadclass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

public class ListenerIN extends AbstractIN{
    public static final String INTERFACE = "StatusListener";
    public ListenerIN(Class<?> c) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IllegalArgumentException {
        super(c, INTERFACE);
    }

    public Collection<String> getStatus() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException{
        Method method = object.getClass().getMethod("getStatuses");
        return (Collection<String>) method.invoke(object);
    }
}
