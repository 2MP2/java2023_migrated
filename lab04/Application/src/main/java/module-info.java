module pl.edu.pwr.pastuszek.application {
    requires javafx.controls;
    requires javafx.fxml;

    opens pl.edu.pwr.pastuszek.application to javafx.fxml;
    exports pl.edu.pwr.pastuszek.application;
}