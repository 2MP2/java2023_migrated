function game(arr) {
  var sum = 0;
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
      sum += arr[i][j];
    }
  }
  if (arr[1][1] === 0) {
    if (sum === 3) {
      return 1;
    } else {
      return 0;
    }
  } else if (arr[1][1] === 1) {
    if (sum >= 5 || sum <= 2) {
      return 0;
    } else {
      return 1;
    }
  }
}