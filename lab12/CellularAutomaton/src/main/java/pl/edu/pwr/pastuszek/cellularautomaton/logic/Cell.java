package pl.edu.pwr.pastuszek.cellularautomaton.logic;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Cell {
    private int state;
    private final Rectangle r;

    public Cell(){
        state=0;
        r=new Rectangle(0,0,8,8);
        r.setFill(Color.WHITE);
        r.setStroke(Color.BLACK);
        r.setStrokeWidth(1);
    }

    public int getState() {
        return state;
    }

    public Rectangle getGraphic() {
        return r;
    }

    public void setState(int state) {
        this.state = state;
        if(state == 0)
            r.setFill(Color.WHITE);
        else
            r.setFill(Color.BLACK);
    }
}
