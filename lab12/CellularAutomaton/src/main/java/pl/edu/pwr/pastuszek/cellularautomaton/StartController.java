package pl.edu.pwr.pastuszek.cellularautomaton;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import pl.edu.pwr.pastuszek.cellularautomaton.logic.Cell;
import pl.edu.pwr.pastuszek.cellularautomaton.logic.EngineJS;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.net.URL;
import java.util.Objects;
import java.util.Random;
import java.util.ResourceBundle;

public class StartController implements Initializable{
    @FXML private GridPane gridPane;
    @FXML private AnchorPane anchorPane;
    @FXML private Button startButton, stopButton;
    private Timeline t;
    private Random random;
    private Cell[][] board;
    private Cell[][] newBoard;
    EngineJS engineJS;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        int rows = 100;
        int col = 100;
        random = new Random();
        board = new Cell[col][rows];
        newBoard = new Cell[col][rows];
        engineJS = new EngineJS();

        EventHandler<ActionEvent> handler = actionEvent -> updateGridPane(board, newBoard);
        t = new Timeline(new KeyFrame(Duration.millis(250),handler));
        t.setCycleCount(Timeline.INDEFINITE);

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < col; j++){
                board[i][j] = new Cell();
                newBoard[i][j] = new Cell();
                gridPane.add(board[i][j].getGraphic(), i, j);
            }
        }


    }

    @FXML
    private void play(){
        feelBoard(board);
        t.play();
    }

    @FXML
    private void stop(){
        t.stop();
    }

    @FXML
    private void loadScript(){
        t.stop();
        final FileChooser fileChooser = new FileChooser();
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        engineJS.loadScript(file);
        if(engineJS.isLoaded()){
            startButton.setDisable(false);
            stopButton.setDisable(false);
        }else {
            startButton.setDisable(true);
            stopButton.setDisable(true);
        }
    }


    public void updateGridPane(Cell[][] board, Cell[][] newBoard){
        int rows = board.length;
        int col = board[0].length;


        for (int i = 1; i < rows - 1; i++) {
            for (int j = 1; j < col -1; j++) {

                int [][] arr = new int[3][3];
                for (int x = -1; x <= 1;x++){
                    for (int y = -1; y <= 1;y++){
                        arr[x+1][y+1]=board[i+x][j+y].getState();
                    }
                }


                try {
                    Object funcResult = engineJS.getValue("game", (Object) arr);
                    newBoard[i][j].setState((Integer) funcResult);
                } catch (ScriptException | NoSuchMethodException e) {
                    throw new RuntimeException(e);
                }

            }
        }

        for (int i = 1; i < rows - 1; i++) {
            for (int j = 1; j < col - 1; j++) {
                board[i][j].setState(newBoard[i][j].getState());
            }
        }

    }

    public void feelBoard(Cell[][] board){
        int rows = board.length;
        int col = board[0].length;

        for (int i = 1; i < rows - 1; i++) {
            for (int j = 1; j < col -1; j++) {
                board[i][j].setState(random.nextInt(2));
            }
        }
    }

}