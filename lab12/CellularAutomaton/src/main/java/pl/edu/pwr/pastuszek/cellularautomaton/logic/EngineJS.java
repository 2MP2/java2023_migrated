package pl.edu.pwr.pastuszek.cellularautomaton.logic;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;
import java.util.Objects;

public class EngineJS {
    private boolean loaded;
    ScriptEngine engine;
    public EngineJS() {
        this.loaded = false;
        this.engine = new ScriptEngineManager().getEngineByName("nashorn");
    }

    public void loadScript(String script){
        try {
            engine.eval(script);
            loaded = true;
        } catch (ScriptException e) {
            loaded=false;
        }
    }

    public void loadScript(File file){
        try(InputStreamReader inputStream = new InputStreamReader(new FileInputStream(file))) {
            engine.eval(inputStream);
            loaded = true;
        } catch (IOException | ScriptException e) {
            loaded =false;
        }
    }

    public Object getValue(String name, Object ... arg) throws ScriptException, NoSuchMethodException {
        if(loaded){
            Invocable invocable = (Invocable) engine;
            return invocable.invokeFunction(name, arg);
        }else{
            throw new IllegalStateException("Didn't load script");
        }

    }

    public boolean isLoaded() {
        return loaded;
    }
}
