module pl.edu.pwr.pastuszek.cellularautomaton {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.openjdk.nashorn;
            
                            
    opens pl.edu.pwr.pastuszek.cellularautomaton to javafx.fxml;
    exports pl.edu.pwr.pastuszek.cellularautomaton;
    exports pl.edu.pwr.pastuszek.cellularautomaton.logic;
}