package pl.edu.pwr.pastuszek.projectxml.parser;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import pl.edu.pwr.pastuszek.projectxml.model.InformationCard;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class JaxPSAXParser extends XmlParser<InformationCard> {
    SAXParserFactory saxParserFactory;
    InformationCardHandler informationCardHandler;
    public JaxPSAXParser() {
        this.saxParserFactory = SAXParserFactory.newInstance();
        this.informationCardHandler = new InformationCardHandler();
    }

    @Override
    public void parsXml(File file) throws IOException, ParseException {
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();
            xmlReader.setContentHandler(informationCardHandler);
            xmlReader.parse(String.valueOf(file));

            cardList.addAll(informationCardHandler.getInformationCards());
            dataReady = true;
        }catch (SAXException | ParserConfigurationException e){
            throw new ParseException(e.getMessage());
        }catch (IOException e){
            throw new IOException(e.getMessage());
        }
    }
}
