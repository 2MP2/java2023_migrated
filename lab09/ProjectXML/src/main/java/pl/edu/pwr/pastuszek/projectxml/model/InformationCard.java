package pl.edu.pwr.pastuszek.projectxml.model;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InformationCard {
    @XmlElement(required = true)
    protected String link = "";

    @XmlElement(required = true)
    protected String id = "";

    @XmlElement(name = "data", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar date;

    @XmlElement(name = "skrot_organizacja", required = true)
    protected String organization;

    @XmlElement(name = "komponent_srodowiska", required = true)
    protected String environmentalComponent;

    @XmlElement(name = "typ_karty", required = true)
    protected String cardType;

    @XmlElement(name = "rodzaj_karty", required = true)
    protected String cardSort;

    @XmlElement(name = "nr_wpisu", required = true)
    protected int reportNumber;

    @XmlElement(name = "znak_sprawy", required = true)
    protected String caseNumber;

    @XmlElement(name = "dane_wnioskodawcy", required = true)
    protected String applicantData;

    @Override
    public String toString() {
        return "InformationCard{" +
                "id='" + id + '\'' +
                '}';
    }

    public String getContent() {
        return  "link=" + link +
                "\nid='" + id +
                "\ndate=" + date +
                "\norganization='" + organization +
                "\nenvironmentalComponent='" + environmentalComponent +
                "\ncardType='" + cardType +
                "\ncardSort='" + cardSort +
                "\nreportNumber=" + reportNumber +
                "\ncaseNumber='" + caseNumber +
                "\napplicantData='" + applicantData;
    }
}
