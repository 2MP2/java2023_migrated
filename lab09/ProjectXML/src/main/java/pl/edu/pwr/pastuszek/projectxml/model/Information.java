package pl.edu.pwr.pastuszek.projectxml.model;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Data;

import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "bip.poznan.pl")
@Data
public class Information {
    @XmlElement(required = true)
    protected Data data;

    @XmlAccessorType(XmlAccessType.FIELD)
    @lombok.Data
    public static class Data {

        @XmlElement(name = "karty_informacyjne", required = true)
        protected InformationCardInfo informationCardInfo;
        

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlRootElement(name = "karty_informacyjne")
        @lombok.Data
        public static class InformationCardInfo {

            @XmlElement(required = true)
            int start;
            @XmlElement(required = true)
            int stop;
            @XmlElement(required = true)
            int size;
            @XmlElement(name = "total_size", required = true)
            int totalSize;

            @XmlElement(required = true)
            protected Items items;

            @XmlAccessorType(XmlAccessType.FIELD)
            @lombok.Data
            public static class Items {

                @XmlElement(name = "karta_informacyjna")
                protected List<InformationCard> informationCards;

            }
        }
    }

}
