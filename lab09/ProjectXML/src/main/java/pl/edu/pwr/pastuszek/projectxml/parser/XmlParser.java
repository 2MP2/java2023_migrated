package pl.edu.pwr.pastuszek.projectxml.parser;

import java.util.ArrayList;
import java.util.List;

public abstract class XmlParser<T> implements XmlParsing<T>{
    protected boolean dataReady;
    protected final List<T> cardList;

    protected XmlParser() {
        this.dataReady = false;
        this.cardList = new ArrayList<>();
    }

    @Override
    public List<T> getList() {
        if (dataReady)
            return cardList;
        else
            throw new IllegalStateException("No data");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
