package pl.edu.pwr.pastuszek.projectxml.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface XmlParsing<T> {
    List<T> getList();
    void parsXml(File file) throws IOException, ParseException;
}
