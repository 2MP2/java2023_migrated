package pl.edu.pwr.pastuszek.projectxml.parser;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import pl.edu.pwr.pastuszek.projectxml.model.InformationCard;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.ArrayList;
import java.util.List;

public class InformationCardHandler extends DefaultHandler {
    private final List<InformationCard> informationCards = new ArrayList<>();

    private InformationCard informationCard = null;

    boolean bLink = false;
    boolean bId = false;
    boolean bDate = false;
    boolean bOrganization = false;
    boolean bEnvironmentalComponent = false;
    boolean bCardType = false;
    boolean bCardSort = false;
    boolean bReportNumber = false;
    boolean bCaseNumber = false;
    boolean bApplicantData = false;

    boolean dataDecoy = false;

    public List<InformationCard> getInformationCards() {
        return informationCards;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch (qName){
            case "karta_informacyjna" -> informationCard = new InformationCard();
            case "link" -> bLink = true;
            case "id" -> bId = true;
            case "data" -> {
                if(dataDecoy)
                    bDate = true;
                else
                    dataDecoy = true;
            }
            case "skrot_organizacja" -> bOrganization = true;
            case "komponent_srodowiska" -> bEnvironmentalComponent = true;
            case "typ_karty" -> bCardType = true;
            case "rodzaj_karty" -> bCardSort = true;
            case "nr_wpisu" -> bReportNumber = true;
            case "znak_sprawy" -> bCaseNumber = true;
            case "dane_wnioskodawcy" -> bApplicantData = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("karta_informacyjna")) {
            informationCards.add(informationCard);
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if(bLink){
            informationCard.setLink(new String(ch,start,length));
            bLink = false;
        }else if(bId){
            informationCard.setId(new String(ch,start,length));
            bId = false;
        }else if(bDate){
            try {
                informationCard.setDate(
                        DatatypeFactory
                                .newInstance()
                                .newXMLGregorianCalendar(new String(ch,start,length))
                );
            } catch (DatatypeConfigurationException e) {
                informationCard.setDate(null);
            }
            bDate = false;
        }else if(bOrganization){
            informationCard.setOrganization(new String(ch,start,length));
            bOrganization = false;
        }else if(bEnvironmentalComponent){
            informationCard.setEnvironmentalComponent(new String(ch,start,length));
            bEnvironmentalComponent = false;
        }else if(bCardType){
            informationCard.setCardType(new String(ch,start,length));
            bCardType = false;
        }else if(bCardSort){
            informationCard.setCardSort(new String(ch,start,length));
            bCardSort = false;
        }else if(bReportNumber){
            informationCard.setReportNumber(Integer.parseInt(new String(ch,start,length)));
            bReportNumber = false;
        }else if(bCaseNumber){
            informationCard.setCardSort(new String(ch,start,length));
            bCaseNumber = false;
        }else if(bApplicantData){
            informationCard.setApplicantData(new String(ch,start,length));
            bApplicantData = false;
        }
    }
}
