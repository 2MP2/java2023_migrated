package pl.edu.pwr.pastuszek.projectxml.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pl.edu.pwr.pastuszek.projectxml.model.InformationCard;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JaxPDOMParser extends XmlParser<InformationCard> {

    // Instantiate the Factory
    DocumentBuilderFactory dbf;

    public JaxPDOMParser() {
        this.dbf = DocumentBuilderFactory.newInstance();
    }

    @Override
    public void parsXml(File file) throws IOException, ParseException {

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            doc.getDocumentElement().normalize();
            List<InformationCard> informationCards = new ArrayList<>();
            NodeList list = doc.getElementsByTagName("karta_informacyjna");
            for (int temp = 0; temp < list.getLength(); temp++) {
                Node node = list.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    informationCards.add(getInformationCardFromElement(element));
                }
            }
            cardList.addAll(informationCards);
            dataReady = true;
        }catch (SAXException | DatatypeConfigurationException | ParserConfigurationException e){
            throw new ParseException(e.getMessage());
        }catch (IOException e){
            throw new IOException(e.getMessage());
        }
    }

    private InformationCard getInformationCardFromElement(Element element) throws DatatypeConfigurationException {
        String link = element.getElementsByTagName("link").item(0).getTextContent();
        String id = element.getElementsByTagName("id").item(0).getTextContent();
        XMLGregorianCalendar date =  DatatypeFactory.newInstance().newXMLGregorianCalendar(
                        element.getElementsByTagName("data").item(0).getTextContent());
        String organiser = element.getElementsByTagName("skrot_organizacja").item(0).getTextContent();
        String description = element.getElementsByTagName("komponent_srodowiska").item(0).getTextContent();
        String cardType = element.getElementsByTagName("typ_karty").item(0).getTextContent();
        String cardSort = element.getElementsByTagName("rodzaj_karty").item(0).getTextContent();
        int reportNumber = Integer.parseInt(element.getElementsByTagName("nr_wpisu").item(0).getTextContent());
        String caseNumber = element.getElementsByTagName("znak_sprawy").item(0).getTextContent();
        String applicantInfo = element.getElementsByTagName("dane_wnioskodawcy").item(0).getTextContent();

        return new InformationCard(
                link,
                id,
                date,
                organiser,
                description,
                cardType,
                cardSort,
                reportNumber,
                caseNumber,
                applicantInfo
        );
    }
}
