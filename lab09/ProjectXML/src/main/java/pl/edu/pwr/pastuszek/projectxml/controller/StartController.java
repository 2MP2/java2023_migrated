package pl.edu.pwr.pastuszek.projectxml.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.edu.pwr.pastuszek.projectxml.model.InformationCard;
import pl.edu.pwr.pastuszek.projectxml.parser.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StartController implements Initializable{
    @FXML private TextArea viewTextArea;
    @FXML private TextField pathTextField;
    @FXML private AnchorPane anchorPane;
    @FXML private ComboBox<XmlParser<InformationCard>> parserComboBox;
    @FXML private ComboBox<InformationCard> informationCardComboBox;
    File file;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        XmlParser<InformationCard> jaxBParser = new JaxBParser();
        XmlParser<InformationCard> jaxPDOMParser = new JaxPDOMParser();
        XmlParser<InformationCard> jaxPSAXParser = new JaxPSAXParser();
        parserComboBox.getItems().addAll(jaxBParser, jaxPDOMParser, jaxPSAXParser);
    }

    @FXML
    private void loadInformationCard(){
        if(!informationCardComboBox.getSelectionModel().isEmpty()){
            viewTextArea.clear();
            viewTextArea.setText(
                    informationCardComboBox
                            .getSelectionModel().getSelectedItem().getContent()
            );
        }
    }
    @FXML
    private void findFile(){
        final FileChooser fileChooser = new FileChooser();
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        file = fileChooser.showOpenDialog(stage);
        try {
            pathTextField.setText(file.getAbsolutePath());
        }catch (NullPointerException e){
            file = null;
        }
    }

    @FXML
    private void loadFile(){
        if(file!=null && parserComboBox.getSelectionModel()!=null){
            try {
                XmlParser<InformationCard> xmlParser= parserComboBox.getSelectionModel().getSelectedItem();
                xmlParser.parsXml(file);
                informationCardComboBox.getItems().clear();
                informationCardComboBox.getItems().addAll(xmlParser.getList());
                viewTextArea.clear();
            } catch (IOException | ParseException e) {
                viewTextArea.setText(e.getMessage());
            }
        }
    }

}