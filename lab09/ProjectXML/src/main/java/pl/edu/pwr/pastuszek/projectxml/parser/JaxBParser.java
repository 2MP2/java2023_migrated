package pl.edu.pwr.pastuszek.projectxml.parser;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import pl.edu.pwr.pastuszek.projectxml.model.Information;
import pl.edu.pwr.pastuszek.projectxml.model.InformationCard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class JaxBParser extends XmlParser<InformationCard>{

    @Override
    public void parsXml(File file) throws ParseException, FileNotFoundException {
        try {
            JAXBContext context = JAXBContext.newInstance(Information.class);
            Information information = (Information) context.createUnmarshaller()
                    .unmarshal(new FileReader(file));
            cardList.addAll(information.getData().getInformationCardInfo().getItems().getInformationCards());
            dataReady = true;
        }catch (JAXBException e){
            throw new ParseException(e.getMessage());
        }catch (FileNotFoundException e){
            throw new FileNotFoundException(e.getMessage());
        }
    }
}
