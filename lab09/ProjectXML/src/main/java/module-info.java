module pl.edu.pwr.pastuszek.projectxml {
    requires javafx.controls;
    requires javafx.fxml;
    requires static lombok;
    requires com.sun.xml.bind;

    opens pl.edu.pwr.pastuszek.projectxml.model to jakarta.xml.bind;

    opens pl.edu.pwr.pastuszek.projectxml to javafx.fxml;
    exports pl.edu.pwr.pastuszek.projectxml;
    exports pl.edu.pwr.pastuszek.projectxml.controller;
    opens pl.edu.pwr.pastuszek.projectxml.controller to javafx.fxml;
}