package pl.edu.pwr.pastuszek;

import com.google.auto.service.AutoService;
import ex.api.AnalysisException;
import ex.api.AnalysisService;
import ex.api.DataSet;

@AutoService(AnalysisService.class)
public class BalancedAccuracy implements AnalysisService {
    private DataSet dataSet = null;
    private boolean isDataProcessing = false;
    private boolean error = false;
    private Double result = null;
    @Override
    public void setOptions(String[] strings) throws AnalysisException {}

    @Override
    public String getName() {
        return "BalancedAccuracy";
    }

    @Override
    public void submit(DataSet dataSet) throws AnalysisException {
        if (isDataProcessing)
            throw new AnalysisException("Dane są jeszcze przetwarzane");
        else
            this.dataSet = dataSet;
        balancedAccuracyAlg();
    }

    @Override
    public Double retrieve(boolean b) throws AnalysisException {
        if(isDataProcessing || dataSet == null)
            return null;

        if(error)
            throw new AnalysisException("Coś poszło nie tak w obliczeniach");

        if(b){
            dataSet = null;
            Double x = result;
            result = null;
            return x;
        }else
            return result;
    }

    private void balancedAccuracyAlg() {
        isDataProcessing = true;
        int numCorrectPositives = 0;
        int numCorrectNegatives = 0;
        int numFalsePositives = 0;
        int numFalseNegatives = 0;

        try {
            String[][] matrix = dataSet.getData();
            int r = matrix.length;
            int c = matrix[0].length;

            int[][] data = new int[r][c];
            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    data[i][j] = Integer.parseInt(matrix[i][j]);
                }
            }

            // Calculate true positives, true negatives, false positives, and false negatives
            for (int i = 0; i < data.length; i++) {
                int truePositives = data[i][i];
                numCorrectPositives += truePositives;
                numCorrectNegatives += sumDiagonal(data) - sumColumn(data, i) - sumRow(data, i) + truePositives - data[i][i];
                numFalsePositives += sumRow(data, i) - truePositives;
                numFalseNegatives += sumColumn(data, i) - truePositives;
            }

            // Calculate balanced accuracy
            double sensitivity = (double) numCorrectPositives / (numCorrectPositives + numFalseNegatives);
            double specificity = (double) numCorrectNegatives / (numCorrectNegatives + numFalsePositives);

            if (Double.isNaN(sensitivity) || Double.isNaN(specificity)) {
                error = true;
            } else {
                double ba = (sensitivity + specificity) / 2;
                if (Double.isFinite(ba)) {
                    result = ba;
                    error = false;
                } else {
                    error = true;
                }
            }

            isDataProcessing = false;

        } catch (Exception e) {
            error = true;
            isDataProcessing = false;
        }
    }



    // Helper function to sum a column of an int[][] matrix
    private int sumColumn(int[][] matrix, int col) {
        int sum = 0;
        for (int[] ints : matrix) {
            sum += ints[col];
        }
        return sum;
    }

    // Helper function to sum a row of an int[][] matrix
    private int sumRow(int[][] matrix, int row) {
        int sum = 0;
        for (int i = 0; i < matrix[0].length; i++) {
            sum += matrix[row][i];
        }
        return sum;
    }

    private int sumDiagonal(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            sum += matrix[i][i];
        }
        return sum;
    }

}
