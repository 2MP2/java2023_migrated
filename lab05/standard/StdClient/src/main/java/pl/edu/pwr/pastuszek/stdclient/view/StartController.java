package pl.edu.pwr.pastuszek.stdclient.view;

import ex.api.AnalysisException;
import ex.api.AnalysisService;
import ex.api.DataSet;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.StringConverter;
import pl.edu.pwr.pastuszek.stdclient.analysis.DataSetTableViewManager;
import pl.edu.pwr.pastuszek.stdclient.analysis.RandomDataSetFabric;
import pl.edu.pwr.pastuszek.stdclient.analysis.ToTableView;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.ServiceLoader;

public class StartController implements Initializable {
    @FXML private TableView<String[]> genMatrixTableView;
    @FXML private ListView<String> resultListView;
    @FXML private TextField sizeGenMatrinxTextField, samplesGenMatrinxTextField;
    @FXML private ComboBox<AnalysisService> algorithmComboBox;
    @FXML private CheckBox resetCheckBox;
    DataSet dataSet;
    @FXML
    private void fillGenMatrixTableView(){
        int size;
        int samples;
        try {
            size = Integer.parseInt(sizeGenMatrinxTextField.getText());
            samples = Integer.parseInt(samplesGenMatrinxTextField.getText());
        }catch (NumberFormatException e){
            return;
        }

        if(size < 2 || samples < 2 || size > 20 || samples > 500)
            return;

        RandomDataSetFabric fabric;
        try {
            fabric = new RandomDataSetFabric(size, samples);
        }catch (IllegalAccessException e) {
            return;
        }

        dataSet = fabric.getDataSet();
        ToTableView<DataSet> tableViewManager = new DataSetTableViewManager();

        genMatrixTableView.setItems(tableViewManager.generateData(dataSet));
        genMatrixTableView.getColumns().setAll(tableViewManager.createColumns(dataSet));
        genMatrixTableView.getColumns().forEach(col -> col.setEditable(true));
        loadDataSetInCB();

    }

    @FXML
    private void loadAlgorithms(){
        algorithmComboBox.getItems().clear();
        ServiceLoader<AnalysisService> services = ServiceLoader.load(AnalysisService.class);

        for (AnalysisService a: services)
            algorithmComboBox.getItems().add(a);

        loadDataSetInCB();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        algorithmComboBox.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(AnalysisService item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getName());
            }
        });

        algorithmComboBox.setConverter(new StringConverter<>() {
            @Override
            public String toString(AnalysisService object) {
                return object == null ? "" : object.getName();
            }

            @Override
            public AnalysisService fromString(String string) {
                return null;
            }
        });


    }

    @FXML
    private void countCoefficients(){
        if(!algorithmComboBox.getSelectionModel().isEmpty()) {
            try {
                Double result = algorithmComboBox.getSelectionModel().getSelectedItem().retrieve(resetCheckBox.isSelected());
                if(result!=null)
                    resultListView.getItems().add(Double.toString(result));
            } catch (AnalysisException e) {
                resultListView.getItems().add(e.toString());
            }
        }
    }

    private void loadDataSetInCB(){
        algorithmComboBox.getItems().forEach(d -> {
            try {
                d.submit(dataSet);
            } catch (AnalysisException e) {
                resultListView.getItems().add(e.toString());
            }
        });
    }
}