package pl.edu.pwr.pastuszek.stdclient.analysis;

import ex.api.DataSet;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;

import java.util.List;
import java.util.stream.IntStream;

public class DataSetTableViewManager implements ToTableView<DataSet>{
    @Override
    public ObservableList<String[]> generateData(DataSet dataSet) {
        return FXCollections.observableArrayList(dataSet.getData());
    }

    @Override
    public List<TableColumn<String[], String>> createColumns(DataSet dataSet) {
        int length = dataSet.getData().length;
        return IntStream.range(0, length)
                .mapToObj(i -> createColumn(i, dataSet.getHeader()))
                .toList();
    }

    private TableColumn<String[], String> createColumn(int c, String[] headers) {
        TableColumn<String[], String> col = new TableColumn<>(headers[c]);
        col.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()[c]));

        return col;
    }


}
