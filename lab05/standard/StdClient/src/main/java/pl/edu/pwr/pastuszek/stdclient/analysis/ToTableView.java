package pl.edu.pwr.pastuszek.stdclient.analysis;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;

import java.util.List;

public interface ToTableView<T> {
    ObservableList<String[]> generateData(T t);
    List<TableColumn<String[], String>> createColumns(T t);
}
