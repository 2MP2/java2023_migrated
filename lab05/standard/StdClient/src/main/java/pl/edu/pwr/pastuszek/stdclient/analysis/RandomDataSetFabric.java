package pl.edu.pwr.pastuszek.stdclient.analysis;

import ex.api.DataSet;

import java.util.Random;

public class RandomDataSetFabric implements DataSetFabric{
    private int size;
    private int samplesSize;
    Random random;
    public RandomDataSetFabric(){
        this.size = 2;
        this.samplesSize = 2;
        this.random = new Random();
    }
    public RandomDataSetFabric(int size, int samplesSize) throws IllegalAccessException {
        if(size >= 2 && samplesSize>=2){
            this.size = size;
            this.samplesSize = samplesSize;
            this.random = new Random();
        }else
            throw new IllegalAccessException("Złe argumenty");
    }
    public void setOption(int size, int samplesSize) throws IllegalAccessException {
        if(size >= 2 && samplesSize>=2){
            this.size = size;
            this.samplesSize = samplesSize;
        }else
            throw new IllegalAccessException("Złe argumenty");

    }
    @Override
    public DataSet getDataSet() {
        DataSet dataSet = new DataSet();
        String[] headers = new String[size];
        String[][] data = new String[size][size];

        for (char i='a'; i < 'a'+size; i++)
            headers[i-'a']=Character.toString(i);
        dataSet.setHeader(headers);

        for(int i = 0; i < size; i++){
            int x = samplesSize;
            for (int j = 0; j < size; j++){
                int rand = random.nextInt(x);
                if(j == size -1)
                    data[i][j] = Integer.toString(x);
                else
                    data[i][j] = Integer.toString(rand);
                x -= rand;
            }
        }
        dataSet.setData(data);

        return dataSet;
    }
}
