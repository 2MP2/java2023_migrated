package pl.edu.pwr.pastuszek.stdclient.analysis;

import ex.api.DataSet;

public interface DataSetFabric {
    DataSet getDataSet();
}
