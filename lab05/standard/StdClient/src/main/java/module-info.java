module pl.edu.pwr.pastuszek.stdclient {
    requires javafx.controls;
    requires javafx.fxml;
    requires Api;
    uses ex.api.AnalysisService;

    opens pl.edu.pwr.pastuszek.stdclient to javafx.fxml;
    exports pl.edu.pwr.pastuszek.stdclient;
    exports pl.edu.pwr.pastuszek.stdclient.view;
    opens pl.edu.pwr.pastuszek.stdclient.view to javafx.fxml;
}