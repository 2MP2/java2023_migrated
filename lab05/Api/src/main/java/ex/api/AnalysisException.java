package ex.api;

import java.io.Serial;

public class AnalysisException extends Exception {
    @Serial
    private static final long serialVersionUID = 1L;
    public AnalysisException(String msg){
        super(msg);
    }
}