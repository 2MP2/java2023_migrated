package org.example;

import ex.api.AnalysisException;
import ex.api.AnalysisService;
import ex.api.DataSet;

public class Accuracy implements AnalysisService{

    private DataSet dataSet = null;
    private boolean isDataProcessing = false;
    private boolean error = false;
    private Double result = null;

    @Override
    public void setOptions(String[] strings) throws AnalysisException {}

    @Override
    public String getName() {
        return "Accuracy";
    }

    @Override
    public void submit(DataSet dataSet) throws AnalysisException {
        if (isDataProcessing)
            throw new AnalysisException("Dane są jeszcze przetwarzane");
        else
            this.dataSet = dataSet;
        accAlg();
    }

    @Override
    public Double retrieve(boolean b) throws AnalysisException {
        if(isDataProcessing || dataSet == null)
            return null;

        if(error)
            throw new AnalysisException("Coś poszło nie tak w obliczeniach");

        if(b){
            dataSet = null;
            Double x = result;
            result = null;
            return x;
        }else
            return result;
    }

    private void accAlg(){
        isDataProcessing = true;
        int numCorrect = 0;
        int numTotal = 0;

        try {
            String[][] matrix = dataSet.getData();
            int r = matrix.length;
            int c = matrix[0].length;


            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    if (i == j) {
                        numCorrect += Integer.parseInt(dataSet.getData()[i][j]);
                    }
                    numTotal += Integer.parseInt(dataSet.getData()[i][j]);
                }
            }


            error = false;
            result = (double) numCorrect / numTotal;
            isDataProcessing = false;
        }catch (Exception e){
            isDataProcessing = false;
            error = true;
        }
    }
}
