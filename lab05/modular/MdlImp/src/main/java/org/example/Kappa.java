package org.example;

import ex.api.AnalysisException;
import ex.api.AnalysisService;
import ex.api.DataSet;

public class Kappa implements AnalysisService {
    private DataSet dataSet = null;
    private boolean isDataProcessing = false;
    private boolean error = false;
    private Double result = null;

    @Override
    public void setOptions(String[] strings) throws AnalysisException {

    }

    @Override
    public String getName() {
        return "Kappa";
    }

    @Override
    public void submit(DataSet dataSet) throws AnalysisException {
        if (isDataProcessing)
            throw new AnalysisException("Dane są jeszcze przetwarzane");
        else
            this.dataSet = dataSet;
        kappaAlg();
    }

    @Override
    public Double retrieve(boolean b) throws AnalysisException {
        if(isDataProcessing || dataSet == null)
            return null;

        if(error)
            throw new AnalysisException("Coś poszło nie tak w obliczeniach");

        if(b){
            dataSet = null;
            Double x = result;
            result = null;
            return x;
        }else
            return result;
    }

    private void kappaAlg() {
        isDataProcessing = true;
        try {
            String[][] matrix = dataSet.getData();
            int r = matrix.length;
            int c = matrix[0].length;

            int[][] data = new int[r][c];
            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    data[i][j] = Integer.parseInt(matrix[i][j]);
                }
            }


            // Calculate the observed agreement
            double observedAgreement = 0;
            int totalObservations = 0;
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[0].length; j++) {
                    totalObservations += data[i][j];
                    if (i == j) {
                        observedAgreement += data[i][j];
                    }
                }
            }
            observedAgreement /= totalObservations;

            // Calculate the expected agreement
            double[] rowTotals = new double[data.length];
            double[] colTotals = new double[data[0].length];
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[0].length; j++) {
                    rowTotals[i] += data[i][j];
                    colTotals[j] += data[i][j];
                }
            }
            double expectedAgreement = 0;
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[0].length; j++) {
                    expectedAgreement += (rowTotals[i] * colTotals[j]) / totalObservations;
                }
            }
            expectedAgreement /= totalObservations;

            // Calculate Cohen's Kappa
            double kappa = (observedAgreement - expectedAgreement) / (1 - expectedAgreement);
            if(Double.isFinite(kappa)){
                result = kappa;
                error = false;
            }else{
                error = true;
            }
            isDataProcessing = false;
        }catch (Exception e) {
            error = true;
            isDataProcessing = false;
        }
    }
}
