module MdlImp {
    requires Api;
    provides ex.api.AnalysisService with org.example.BalancedAccuracy, org.example.Accuracy, org.example.Kappa;
    exports org.example;
}