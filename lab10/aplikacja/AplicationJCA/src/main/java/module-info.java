module pl.edu.pwr.pastuszek.aplicationjca {
    requires javafx.controls;
    requires javafx.fxml;
    requires LibraryJCA;
                            
    opens pl.edu.pwr.pastuszek.aplicationjca to javafx.fxml;
    exports pl.edu.pwr.pastuszek.aplicationjca;
}