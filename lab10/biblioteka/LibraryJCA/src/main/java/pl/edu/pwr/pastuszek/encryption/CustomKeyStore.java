package pl.edu.pwr.pastuszek.encryption;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public class CustomKeyStore {
    private static final CustomKeyStore customKeyStore;
    private static final String KEY_STORE_TYPE = "PKCS12";

    private CustomKeyStore() {}

    static {
        customKeyStore = new CustomKeyStore();
    }

    public static CustomKeyStore getCustomKeyStore() {
        return CustomKeyStore.customKeyStore;
    }

    public void saveInStoreSecKey(File file, char[] password, String alias, SecretKey secretKey) throws IOException, GeneralSecurityException {
        KeyStore store = KeyStore.getInstance(KEY_STORE_TYPE);
        store.load(null,null);
        KeyStore.SecretKeyEntry aesKey = new KeyStore.SecretKeyEntry(secretKey);
        store.setEntry(alias, aesKey, new KeyStore.PasswordProtection(password));
        try(FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            store.store(fileOutputStream,password);
        }
    }

    public SecretKey readFromStoreSecKey(File file, char[] password, String alias) throws IOException, GeneralSecurityException{
        KeyStore store = KeyStore.getInstance(KEY_STORE_TYPE);
        try(FileInputStream inputStream = new FileInputStream(file)) {
            store.load(inputStream, password);
        }
        KeyStore.SecretKeyEntry entry = (KeyStore.SecretKeyEntry)
                store.getEntry(alias, new KeyStore.PasswordProtection(password));
        return entry.getSecretKey();
    }

    public PrivateKey getPrivateKeyFromCertificate(File keystoreFile, char[] keystorePassword, String alias, char[] keyPassword) throws IOException, GeneralSecurityException {
        KeyStore keyStore = KeyStore.getInstance(KEY_STORE_TYPE);

        try (FileInputStream inputStream = new FileInputStream(keystoreFile)) {
            keyStore.load(inputStream, keystorePassword);
        }

        Key key = keyStore.getKey(alias, keyPassword);
        if (key instanceof PrivateKey) {
            return (PrivateKey) key;
        }

        throw new UnrecoverableKeyException("Private key not found in the keystore for the specified alias.");
    }

    public X509Certificate getX509CertificateFromKeyStore(File keystoreFile, char[] keystorePassword, String alias) throws IOException, GeneralSecurityException {
        KeyStore keyStore = KeyStore.getInstance(KEY_STORE_TYPE);

        try (FileInputStream inputStream = new FileInputStream(keystoreFile)) {
            keyStore.load(inputStream, keystorePassword);
        }

        Certificate certificate = keyStore.getCertificate(alias);
        if (certificate instanceof X509Certificate) {
            return (X509Certificate) certificate;
        }

        throw new IllegalArgumentException("Certificate is not an instance of X509Certificate.");
    }

}