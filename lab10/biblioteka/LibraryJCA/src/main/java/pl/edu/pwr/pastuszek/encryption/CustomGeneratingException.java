package pl.edu.pwr.pastuszek.encryption;

import java.io.Serial;

public class CustomGeneratingException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = 1L;
    public CustomGeneratingException(String message) {
        super(message);
    }
}
