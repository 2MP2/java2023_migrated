package pl.edu.pwr.pastuszek.encryption;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;

public class RSAEncryption {
    private static final String TRANSFORMATION = "RSA";
    public byte[] encrypt(byte[] content, PublicKey publicKey) throws GeneralSecurityException {
        Cipher encryptCipher = Cipher.getInstance(TRANSFORMATION);
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return encryptCipher.doFinal(content);
    }

    public byte[] decrypt(byte[] content, PrivateKey privateKey) throws GeneralSecurityException {
        Cipher decryptCipher = Cipher.getInstance(TRANSFORMATION);
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
        return decryptCipher.doFinal(content);
    }

    public SecretKey decryptRSAtoSecretKey(byte[] encryptedKey, PrivateKey privateKey) throws GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedKey);
        return new SecretKeySpec(decryptedBytes, "AES");
    }

    public IvParameterSpec decryptRSAtoIv(byte[] encryptedKey, PrivateKey privateKey) throws GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedKey);
        return new IvParameterSpec(decryptedBytes);
    }
}
