package pl.edu.pwr.pastuszek.encryption;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.IOException;
import java.security.*;

public class CustomKeyGenerator {
    private boolean initPrvPub;
    private KeyPair pair;

    public CustomKeyGenerator() {
        initPrvPub = false;
    }

    public IvParameterSpec generateIv() {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return new IvParameterSpec(iv);
    }

    public SecretKey genSecretKeyASC() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128);
        return keyGenerator.generateKey();
    }

    public void genKeyRSA() throws CustomGeneratingException {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(2048);
            pair = generator.generateKeyPair();
            initPrvPub = true;
        }catch (GeneralSecurityException e){
            initPrvPub = false;
            throw new CustomGeneratingException(e.getMessage());
        }

    }

    public void customCertificateGen(String alias, File keystore, String password) throws CustomGeneratingException {
        if(password.length() < 6)
            throw new CustomGeneratingException("Password to short");

        try {
            Process process = initKeytoolGen(alias, keystore).start();

            if(!keystore.exists())
                process.getOutputStream().write((password + "\n").getBytes());

            process.getOutputStream().write((password + "\n").getBytes());
            process.getOutputStream().write("\n\n\n\n\n\n".getBytes());
            process.getOutputStream().write("yes\n".getBytes());
            process.getOutputStream().flush();

            int exitCode = process.waitFor();
            process.getOutputStream().close();

            if (exitCode != 0)
                throw new CustomGeneratingException("Problem with method keytool");

        } catch (IOException | InterruptedException e) {
            throw new CustomGeneratingException(e.getMessage());
        }
    }
    public void customCertificateGen(String alias, File keystore, String password,
                                     String firstName, String lastName, String organizationalUnit, String organization, String city, String state, String country) throws CustomGeneratingException {

        try {
            Process process = initKeytoolGen(alias, keystore).start();

            if(!keystore.exists())
                process.getOutputStream().write((password + "\n").getBytes());

            process.getOutputStream().write((password + "\n").getBytes());
            process.getOutputStream().write((firstName + " " + lastName + "\n").getBytes());
            process.getOutputStream().write((organizationalUnit + "\n").getBytes());
            process.getOutputStream().write((organization + "\n").getBytes());
            process.getOutputStream().write((city + "\n").getBytes());
            process.getOutputStream().write((state + "\n").getBytes());
            process.getOutputStream().write((country + "\n").getBytes());

            process.getOutputStream().write("yes\n".getBytes());
            process.getOutputStream().flush();

            int exitCode = process.waitFor();
            process.getOutputStream().close();

            if (exitCode != 0)
                throw new CustomGeneratingException("Problem with method keytool");

        } catch (IOException | InterruptedException e) {
            throw new CustomGeneratingException(e.getMessage());
        }
    }

    public void deleteCertificate(String alias, File keystorePath, String password) throws CustomGeneratingException {
        if(password.length() < 6)
            throw new CustomGeneratingException("Password to short");

        try {
            Process process = initKeytoolDelete(alias, keystorePath).start();
            process.getOutputStream().write((password + "\n").getBytes());
            process.getOutputStream().flush();

            int exitCode = process.waitFor();
            process.getOutputStream().close();

            if (exitCode != 0) {
                throw new CustomGeneratingException("Problem with method keytool");
            }

        } catch (IOException | InterruptedException e) {
            throw new CustomGeneratingException(e.getMessage());
        }
    }

    private ProcessBuilder initKeytoolDelete(String alias, File keystorePath) {
        return new ProcessBuilder(
                "keytool",
                "-delete",
                "-alias", alias,
                "-keystore", keystorePath.getAbsolutePath()
        );
    }


    private ProcessBuilder initKeytoolGen(String alias, File keystorePath){
        return new ProcessBuilder(
                "keytool",
                "-genkeypair",
                "-alias", alias,
                "-keyalg", "RSA",
                "-validity", "365",
                "-keysize", "2048",
                "-keystore", keystorePath.getAbsolutePath()
        );
    }

    public PrivateKey getPrivateKey() {
        if(initPrvPub)
            return pair.getPrivate();
        else
            throw new IllegalArgumentException("Key wasn't generated");
    }

    public PublicKey getPublicKey() {
        if(initPrvPub)
            return pair.getPublic();
        else
            throw new IllegalArgumentException("Key wasn't generated");
    }
}
