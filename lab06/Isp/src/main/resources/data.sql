INSERT INTO client (Id, name, surname, phone_Number) VALUES (NEXTVAL('client_sequence'),'Jan', 'Kowalski', '123456789');
INSERT INTO client (Id, name, surname, phone_Number) VALUES (NEXTVAL('client_sequence'),'Anna', 'Nowak', '987654321');
INSERT INTO client (Id, name, surname, phone_Number) VALUES (NEXTVAL('client_sequence'),'Katarzyna', 'Wiśniewska', '555333777');

INSERT INTO price_list(Id, name, service_type, price)
VALUES (NEXTVAL('price_list_sequence'),'Basic', 'ACTIVE', 10.0),
       (NEXTVAL('price_list_sequence'),'Premium', 'EXPIRED', 20.0),
       (NEXTVAL('price_list_sequence'),'Advanced', 'NEWLY_ESTABLISHED', 30.0);

INSERT INTO installation (Id, address, router_Number, is_Active, client_id, price_list_id)
VALUES (NEXTVAL('installation_sequence'),'ul. Wiejska 1', 123456789, true, 1, 1),
       (NEXTVAL('installation_sequence'),'ul. Słoneczna 2', 987654321, false, 2, 2),
       (NEXTVAL('installation_sequence'),'ul. Polna 3', 555333777, true, 3, 3);

