package pl.edu.pwr.pastuszek.isp.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "payment")
public class Payment {
    @Id
    @SequenceGenerator(
            name = "payment_sequence",
            sequenceName = "payment_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "payment_sequence"
    )
    private Long id;
    @Column(nullable = false)
    private LocalDate dateOfPayment;
    @Column(nullable = false)
    private Double amountPaid;
    @ManyToOne
    @JoinColumn(name = "account_receivable_id")
    private AccountReceivable accountReceivable;

    @Override
    public String toString() {
        return this.id + " " + this.dateOfPayment + " " + String.format("%f.2",this.amountPaid) + " " + this.accountReceivable.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Objects.equals(id, payment.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
