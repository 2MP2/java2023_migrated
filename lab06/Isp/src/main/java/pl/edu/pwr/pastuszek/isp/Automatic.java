package pl.edu.pwr.pastuszek.isp;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.edu.pwr.pastuszek.isp.model.AccountReceivable;
import pl.edu.pwr.pastuszek.isp.model.Installation;
import pl.edu.pwr.pastuszek.isp.service.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class Automatic {
    private final AccountReceivableService accountReceivableService;
    private final ClientService clientService;
    private final InstallationService installationService;
    private final PaymentService paymentService;
    private final PriceListService priceListService;
    private final UserCI userCI = new UserCI();
    private LocalDate actualDate = LocalDate.now();
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public void incrementActualDate(Long days){
        actualDate = actualDate.plusDays(days);
    }

    private void sendRemainderToPay(){
        List<Installation> installations = installationService.getInstallations();
        for(Installation i: installations){
            if(Boolean.TRUE.equals(i.getIsActive())){

                AccountReceivable accountReceivable =
                        accountReceivableService.getARByIdInstallSortDateMax(i.getId());

                if(accountReceivable !=null){
                    LocalDate temDate = accountReceivable.getDateOfDeadline().plusMonths(1);
                    if(actualDate.isEqual(temDate)){
                        creatNewAccountReceivableAndAddToDB(i);
                    }
                }else{
                    creatNewAccountReceivableAndAddToDB(i);
                }

            }
        }
    }
    private AccountReceivable creatNewAccountReceivableAndAddToDB(Installation installation){
        AccountReceivable accountReceivable = AccountReceivable.builder()
                .dateOfDeadline(actualDate.plusMonths(1))
                .amountToPay(installation.getPriceList().getPrice())
                .installation(installation)
                .build();
        accountReceivableService.addAccountReceivable(accountReceivable);
        return accountReceivable;
    }

    private void sendRemainderToPayDeb(){
        List<AccountReceivable> accountReceivables = accountReceivableService.getByPaymentsIsNull();
        if(accountReceivables != null){
            for(AccountReceivable a: accountReceivables)
                if(actualDate.isAfter(a.getDateOfDeadline())){
                    try {
                        addToFile(
                                "Zapłać za " + a + "\n"
                        );
                    }catch (IOException e){}
                }
        }

    }

    private void addToFile(String content) throws IOException {
        Path filePath = Path.of("C:\\Users\\mikol\\OneDrive\\Dokumenty\\JAVA\\sem6\\repPWJJ\\mikpas138_javata_2023\\lab06\\Isp\\src\\main\\resources\\plik.txt");
        Files.writeString(filePath, content, StandardOpenOption.APPEND);

    }

    public void start(){
        // tu puszczamy pętle w nowym wątku która imituje wszsytko
        executorService.scheduleAtFixedRate(
                ()->{
                    sendRemainderToPay();
                    sendRemainderToPayDeb();
                    incrementActualDate(1L);
                },
                1, 1, TimeUnit.SECONDS);
    }

    public void stop(){
        executorService.shutdown();
    }

    public void startUserInterface(){
        boolean flag = true;
        while (flag){
            System.out.println(
                    """
                    Wybierz opcję:
                    1. Wypisz
                    2. Dodaj
                    3. Edytuj
                    4. Wyjdź"""
            );
            int choice = userCI.getIntFromUser();
            switch (choice) {
                case 1 -> {
                    System.out.println(userCI.getOption());
                    choice = userCI.getIntFromUser();
                    switch (choice) {
                        case 1 -> clientService.getClients().forEach(System.out::println);
                        case 2 -> installationService.getInstallations().forEach(System.out::println);
                        case 3 -> priceListService.getPriceLists().forEach(System.out::println);
                        case 4 -> accountReceivableService.getAccountReceivables().forEach(System.out::println);
                        case 5 -> paymentService.getPayments().forEach(System.out::println);
                        default -> System.out.println("Nie ma takiej opcji");
                    }
                }
                case 2 -> {
                    System.out.println(userCI.getOption());
                    choice = userCI.getIntFromUser();
                    switch (choice) {
                        case 1 -> clientService.addClient(userCI.creatClient());
                        case 2 -> installationService.addInstallation(userCI.creatInstallation());
                        case 3 -> priceListService.addPriceList(userCI.creatPriceList());
                        case 4 -> System.out.println("Nie masz uprawnień");
                        case 5 -> paymentService.addPayment(userCI.creatPayment());
                        default -> System.out.println("Nie ma takiej opcji");
                    }
                }
                case 3 -> {
                    System.out.println(userCI.getOption());
                    choice = userCI.getIntFromUser();
                    switch (choice) {
                        case 1 -> editClient();
                        case 2 -> editInstallation();
                        case 3 -> editPriceList();
                        case 4 -> System.out.println("Nie masz uprawnień");
                        case 5 -> editPayment();
                        default -> System.out.println("Nie ma takiej opcji");
                    }
                }
                case 4 -> flag = false;
                default -> System.out.println("Nie ma takiej opcji");
            }
        }

    }

    public void editClient(){
        System.out.print("Podaj id klienta: ");
        Long id = userCI.getLongFromUser();
        if(id == 0)
            return;
        System.out.print("Podaj nowe imię: ");
        String name = userCI.getStringFromUser();
        System.out.print("Podaj nowe nazwisko: ");
        String surname = userCI.getStringFromUser();
        System.out.print("Podaj nowy numertelefonu: ");
        String phoneNumber = userCI.getStringFromUser();

        clientService.updateClient(id,
                name.equals("0") ? null : name,
                surname.equals("0") ? null : surname,
                phoneNumber.equals("0") ? null : phoneNumber);
    }

    public void editInstallation(){
        System.out.print("Podaj id instalacji: ");
        Long id = userCI.getLongFromUser();
        if(id == 0)
            return;
        System.out.print("Podaj nowy adres: ");
        String address = userCI.getStringFromUser();
        System.out.print("Podaj nowy numer rutera: ");
        Long nr = userCI.getLongFromUser();
        System.out.print("""
                Wybierz opcję 
                1. Aktywacja
                2. Dezaktywacja""");
        Integer intFromUser = userCI.getIntFromUser();
        Boolean choice = intFromUser == 1 ? Boolean.TRUE
                : (intFromUser == 2 ? Boolean.FALSE : null);
        System.out.print("Podaj nowe id cennika: ");
        Long idPriceList = userCI.getLongFromUser();

        installationService.updateInstallation(id,
                address.equals("0") ? null : address,
                nr == 0L ? null: nr,
                choice,
                idPriceList == 0L ? null : idPriceList);
    }

    public void editPriceList(){
        System.out.print("Podaj id cennika: ");
        Long id = userCI.getLongFromUser();
        if(id == 0)
            return;
        System.out.print("Podaj nową nazwę: ");
        String name = userCI.getStringFromUser();
        System.out.print("Podaj nową cene: ");
        Double price = userCI.getDoubleFromUser();

        priceListService.updatePriceList(id,
                name.equals("0") ? null : name,
                null,
                price == 0d ? null : price);
    }

    public void editPayment(){
        System.out.print("Podaj id płatności: ");
        Long id = userCI.getLongFromUser();
        if(id == 0)
            return;
        System.out.println("Podaj nową datę: ");
        LocalDate localDate = userCI.getDateFromUser();
        System.out.print("Podaj nową kwotę: ");
        Double price = userCI.getDoubleFromUser();

        paymentService.updatePayment(id,
                localDate.getYear() == 0? null : localDate,
                price == 0d ? null : price);

    }

}
