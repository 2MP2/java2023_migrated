package pl.edu.pwr.pastuszek.isp.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.pastuszek.isp.model.Client;
import pl.edu.pwr.pastuszek.isp.repository.ClientRepository;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class ClientService {
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> getClients(){
        return clientRepository.findAll();
    }

    public Optional<Client> getClientById(Long id){
        return clientRepository.findById(id);
    }

    public void addClient(Client client){
        clientRepository.save(client);
    }

    public void updateClient(Long id, String name, String surname, String phoneNumber){
        Client client = clientRepository.findById(id)
                .orElseThrow( () -> new IllegalStateException(
                        "client with id: " + id + " dose not exists"
                ));

        if(name != null && name.length() >0)
            client.setName(name);
        if(surname != null && surname.length() >0)
            client.setSurname(surname);
        if(phoneNumber != null && phoneNumber.length() >0)
            client.setPhoneNumber(phoneNumber);
    }

    public void deleteClient(Long id){
        boolean exists = clientRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("client with id: " + id + " dose not exists");
        }
        clientRepository.deleteById(id);
    }

}
