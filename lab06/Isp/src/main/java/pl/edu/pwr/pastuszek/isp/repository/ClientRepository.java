package pl.edu.pwr.pastuszek.isp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwr.pastuszek.isp.model.Client;
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
}
