package pl.edu.pwr.pastuszek.isp.model;

public enum ServiceType {
    ACTIVE, EXPIRED, NEWLY_ESTABLISHED
}
