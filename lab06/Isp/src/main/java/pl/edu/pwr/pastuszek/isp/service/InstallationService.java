package pl.edu.pwr.pastuszek.isp.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.pastuszek.isp.model.Installation;
import pl.edu.pwr.pastuszek.isp.model.PriceList;
import pl.edu.pwr.pastuszek.isp.repository.InstallationRepository;
import pl.edu.pwr.pastuszek.isp.repository.PriceListRepository;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class InstallationService {
    private final InstallationRepository installationRepository;
    private final PriceListRepository priceListRepository;

    @Autowired
    public InstallationService(InstallationRepository installationRepository, PriceListRepository priceListRepository) {
        this.installationRepository = installationRepository;
        this.priceListRepository = priceListRepository;
    }

    public List<Installation> getInstallations(){
        return installationRepository.findAll();
    }

    public Optional<Installation> getInstallationById(Long id){
        return installationRepository.findById(id);
    }
    public void addInstallation(Installation installation){
        installationRepository.save(installation);
    }

    @Transactional
    public void updateInstallation(Long id, String address, Long routerNumber, Boolean isActive, Long priceListId){
        Installation installation = installationRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException(
                        "installation with id: " + id + " dose not exists"
                ));
        if(address != null && address.length()>0)
            installation.setAddress(address);
        if(routerNumber != null)
            installation.setRouterNumber(routerNumber);
        if(isActive != null)
            installation.setIsActive(isActive);
        if(priceListId != null){
            PriceList priceList = priceListRepository.findById(priceListId)
                    .orElseThrow(() -> new IllegalStateException(
                            "price list with id:" + priceListId + "dose not exists"
                    ));
            installation.setPriceList(priceList);
        }

    }

    public void deleteInstallation(Long id){
        boolean exists = installationRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("installation with id: " + id + " dose not exists");
        }
        installationRepository.deleteById(id);
    }

}
