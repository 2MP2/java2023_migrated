package pl.edu.pwr.pastuszek.isp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwr.pastuszek.isp.model.AccountReceivable;

import java.util.List;

@Repository
public interface AccountReceivableRepository extends JpaRepository<AccountReceivable, Long> {
    AccountReceivable findFirstByInstallationIdOrderByDateOfDeadlineDesc(Long installationId);
    List<AccountReceivable> findByPaymentsIsNull();

}
