package pl.edu.pwr.pastuszek.isp;

import lombok.Getter;
import pl.edu.pwr.pastuszek.isp.model.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.Scanner;

@Getter
public class UserCI {
    private String option = """
            1. Klienta
            2. Instalacje
            3. Cenniki
            4. Naliczone Płatności
            5. Płatności""";
    private final Scanner scanner;

    public UserCI(){
        scanner = new Scanner(System.in);
    }

    public Integer getIntFromUser() throws IllegalArgumentException{
        int integer ;
        try {
            integer = scanner.nextInt();
        }catch (NoSuchElementException | IllegalStateException ex){
            throw new IllegalArgumentException(ex);
        }finally {
            scanner.nextLine();
        }
        return integer;
    }
    public Double getDoubleFromUser() throws IllegalArgumentException{
        double db;
        try {
            db = scanner.nextDouble();
        }catch (NoSuchElementException | IllegalStateException ex){
            throw new IllegalArgumentException(ex);
        }finally {
            scanner.nextLine();
        }
        return db;
    }
    public Long getLongFromUser() throws IllegalArgumentException{
        long aLong;
        try {
            aLong = scanner.nextLong();
        }catch (NoSuchElementException | IllegalStateException ex){
            throw new IllegalArgumentException(ex);
        }finally {
            scanner.nextLine();
        }
        return aLong;
    }
    public String getStringFromUser() throws IllegalArgumentException{
        String string;
        try {
            string = scanner.nextLine();
        }catch (NoSuchElementException | IllegalStateException ex){
            throw new IllegalArgumentException(ex);
        }
        return string;
    }
    public LocalDate getDateFromUser() throws IllegalArgumentException{
        System.out.print("Podaj rok: ");
        Integer year = getIntFromUser();
        System.out.print("Podaj miesiąc (liczbowo): ");
        Integer month = getIntFromUser();
        System.out.print("Podaj dzień (liczbowo): ");
        Integer day = getIntFromUser();

        try {
            return LocalDate.of(year, month, day);
        }catch (DateTimeException ex){
            throw new IllegalArgumentException(ex);
        }
    }

    public Client creatClient(){
        System.out.print("Podaj imie: ");
        String name = getStringFromUser();
        System.out.print("Podaj nazwisko: ");
        String surname = getStringFromUser();
        System.out.print("Podaj numer telefonu: ");
        String phoneNumber = getStringFromUser();

        return Client.builder().name(name).surname(surname).phoneNumber(phoneNumber).build();
    }

    public Installation creatInstallation(){
        System.out.print("Podaj adres: ");
        String address = getStringFromUser();
        System.out.print("Podaj numer rutera: ");
        Long routerNumber = getLongFromUser();
        System.out.print("Podaj id klienta: ");
        Long idClient = getLongFromUser();
        System.out.print("Podaj id cennika: ");
        Long idPriceList = getLongFromUser();


        return Installation.builder().address(address).routerNumber(routerNumber)
                .client(Client.builder().id(idClient).build())
                .priceList(PriceList.builder().id(idPriceList).build()).build();
    }

    public PriceList creatPriceList(){
        System.out.print("Podaj nazwę: ");
        String name = getStringFromUser();
        System.out.println("""
                Wybierz typ usługi:
                1. AKTYWNY
                2. NOWA
                3. WYGASŁĄ""");
        Integer choice = getIntFromUser();
        ServiceType serviceType = choice==2 ? ServiceType.NEWLY_ESTABLISHED
                : (choice==3 ? ServiceType.EXPIRED : ServiceType.ACTIVE);
        System.out.print("Podaj cenę: ");
        Double price = getDoubleFromUser();

         return PriceList.builder().name(name).price(price)
                 .serviceType(serviceType).build();
    }

    public Payment creatPayment(){
        System.out.println("Podaj datę zapłaty: ");
        LocalDate localDate = getDateFromUser();
        System.out.print("Podaj kowtę: ");
        Double price = getDoubleFromUser();
        System.out.print("Podaj id należności: ");
        Long idAccountReceivable = getLongFromUser();

        return Payment.builder().dateOfPayment(localDate).amountPaid(price)
                .accountReceivable(AccountReceivable.builder().id(idAccountReceivable).build()).build();
    }



}
