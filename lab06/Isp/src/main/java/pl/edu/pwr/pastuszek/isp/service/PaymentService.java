package pl.edu.pwr.pastuszek.isp.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.pastuszek.isp.model.Payment;
import pl.edu.pwr.pastuszek.isp.repository.PaymentRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public List<Payment> getPayments(){
        return paymentRepository.findAll();
    }

    public Optional<Payment> getPaymentById(Long id){
        return paymentRepository.findById(id);
    }
    public void addPayment(Payment payment){
        paymentRepository.save(payment);
    }

    @Transactional
    public void updatePayment(Long id, LocalDate dateOfPayment, Double amountPaid){
        Payment payment = paymentRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException(
                   "payment with id: " + id + " dose not exists"
                ));

        if(dateOfPayment != null)
            payment.setDateOfPayment(dateOfPayment);
        if(amountPaid !=null && amountPaid>0)
            payment.setAmountPaid(amountPaid);
    }

    public void deletePayment(Long id){
        boolean exists = paymentRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("payment with id: " + id + " dose not exists");
        }
        paymentRepository.deleteById(id);
    }
}
