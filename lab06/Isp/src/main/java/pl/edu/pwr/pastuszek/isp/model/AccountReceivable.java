package pl.edu.pwr.pastuszek.isp.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "account_receivable")
public class AccountReceivable {
    @Id
    @SequenceGenerator(
            name = "account_receivable_sequence",
            sequenceName = "account_receivable_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "account_receivable_sequence"
    )
    private Long id;
    @Column(nullable = false)
    private LocalDate dateOfDeadline;
    @Column(nullable = false)
    private Double amountToPay;
    @ManyToOne
    @JoinColumn(name = "installation_id")
    private Installation installation;
    @OneToMany(mappedBy = "accountReceivable")
    private List<Payment> payments;

    @Override
    public String toString() {
        return this.id + " " + this.dateOfDeadline + " " + this.amountToPay + " " + this.installation.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountReceivable that = (AccountReceivable) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
