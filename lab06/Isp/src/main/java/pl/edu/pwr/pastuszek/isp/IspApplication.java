package pl.edu.pwr.pastuszek.isp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IspApplication implements CommandLineRunner {
    private final Automatic automatic;

    @Autowired
    public IspApplication(Automatic automatic) {
        this.automatic = automatic;
    }

    public static void main(String[] args) {
        SpringApplication.run(IspApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        automatic.start();
        automatic.startUserInterface();
        automatic.stop();
    }
}
