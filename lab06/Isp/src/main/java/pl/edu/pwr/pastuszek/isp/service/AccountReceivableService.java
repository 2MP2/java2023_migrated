package pl.edu.pwr.pastuszek.isp.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.pastuszek.isp.model.AccountReceivable;
import pl.edu.pwr.pastuszek.isp.repository.AccountReceivableRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class AccountReceivableService {
    private final AccountReceivableRepository accountReceivableRepository;

    @Autowired
    public AccountReceivableService(AccountReceivableRepository accountReceivableRepository) {
        this.accountReceivableRepository = accountReceivableRepository;
    }

    public List<AccountReceivable> getAccountReceivables(){
        return accountReceivableRepository.findAll();
    }

    public Optional<AccountReceivable> getAccountReceivableById(Long id){
        return accountReceivableRepository.findById(id);
    }

    public void addAccountReceivable(AccountReceivable accountReceivable){
        accountReceivableRepository.save(accountReceivable);
    }


    public void updateAccountReceivable(Long id, LocalDate dateOfDeadline, Double amountToPay){
        AccountReceivable accountReceivable = accountReceivableRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException(
                        "account receivable with id: " + id + " dose not exists"
                ));

        if(dateOfDeadline !=null)
            accountReceivable.setDateOfDeadline(dateOfDeadline);
        if(amountToPay != null && amountToPay > 0d)
            accountReceivable.setAmountToPay(amountToPay);
    }

    public void deleteAccountReceivable(Long id){
        boolean exists = accountReceivableRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("account receivable with id:" + id + " dose not exists");
        }
        accountReceivableRepository.deleteById(id);
    }

    public AccountReceivable getARByIdInstallSortDateMax(Long id){
        return accountReceivableRepository
                .findFirstByInstallationIdOrderByDateOfDeadlineDesc(id);
    }

    public List<AccountReceivable> getByPaymentsIsNull(){
        return accountReceivableRepository.findByPaymentsIsNull();
    }
}
