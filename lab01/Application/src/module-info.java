module Application {
    requires Library;
    requires org.apache.commons.codec;

    requires javafx.fxml;
    requires javafx.controls;
    opens pl.edu.pwr;
    opens pl.edu.pwr.controllers;

}