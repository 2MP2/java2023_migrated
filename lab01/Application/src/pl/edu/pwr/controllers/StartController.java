package pl.edu.pwr.controllers;

import filetree.SnapshotFileTreeMaker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StartController {

@FXML TextField browseTextField1, browseTextField2;
@FXML Label errorLabel;
@FXML Button browseButton1, browseButton2, startButton;
@FXML AnchorPane anchorPane;

private final String directoryName = ".md5";

    private void browseDirectory(TextField textField){
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        File file = directoryChooser.showDialog(stage);

        if(file !=null && file.isDirectory()){
            textField.setText(file.getAbsolutePath());
        }
    }

    @FXML
    private void findFirstDirectory(ActionEvent actionEvent) throws IOException{
        browseDirectory(browseTextField1);
    }

    @FXML
    private void findSecondDirectory(ActionEvent actionEvent) throws IOException{
        browseDirectory(browseTextField2);
    }

    @FXML
    private void startCopyFillTree(ActionEvent actionEvent) throws IOException{
        Path sourceRoot = Paths.get(browseTextField1.getText()).normalize();
        Path targetRoot = Paths.get(browseTextField2.getText()).normalize();

        if(Files.isDirectory(sourceRoot) && Files.isDirectory(targetRoot)){
            errorLabel.setText("Wait, program is processing");
            errorLabel.setVisible(true);

            if(!targetRoot.getFileName().toString().equals(directoryName)){ //byc moze tzeba dodac opcje gdy juz istanieje ale uzytkownik go nie wezmie
                targetRoot = targetRoot.resolve(directoryName);
                //Files.createDirectories(targetRoot);
            }

            SnapshotFileTreeMaker.creatSnapshotFileTree(sourceRoot,targetRoot);

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/MainView.fxml"));
            Parent root = fxmlLoader.load();

            MainController mainController = fxmlLoader.getController();
            mainController.initDate(sourceRoot, targetRoot, sourceRoot);

            Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }else{
            errorLabel.setText("ERROR");
            errorLabel.setVisible(true);
        }


    }


}
