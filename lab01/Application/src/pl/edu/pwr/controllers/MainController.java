package pl.edu.pwr.controllers;

import filetree.Snapshot;
import filetree.SnapshotFileTreeMaker;
import filetree.SnapshotOperator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class MainController implements Initializable{

@FXML private Label pathLabel;
@FXML private Button previousButton, goNextDirectory;
@FXML private ListView<String> fillListView,directoryListView,deleteDirectoryListView;

private Path sourceRoot, targetCurrentPath, currentPath;
private Snapshot oldSnapshot, newSnapshot;

private enum Suffix{
        OLD, CHG, NEW, DEL
    }

    private void addMapToListView(Map<String,String> map, ListView<String> listView, Suffix suffix){
        map.keySet().forEach(s -> listView.getItems().add(s + "?" + suffix.name() + "?"));
    }

    private void addSetToListView(Set<String> set, ListView<String> listView, Suffix suffix){
        set.forEach(s -> listView.getItems().add(s + "?" + suffix.name() + "?"));
    }
    private void initListViews(Path sourceRoot, Path targetCurrentPath){
        try {
            newSnapshot = SnapshotOperator.createSnapshot(sourceRoot);
            oldSnapshot = SnapshotOperator.readSnapshotFromDirectory(targetCurrentPath);

            Map<String,String> odlFileMap = SnapshotOperator.getNotChangedFiles(oldSnapshot,newSnapshot);
            Map<String,String> changedFileMap = SnapshotOperator.getChangedFiles(oldSnapshot,newSnapshot);
            Map<String,String> newFileMap = SnapshotOperator.getNewFiles(oldSnapshot,newSnapshot);
            Map<String,String> deleteFileMap = SnapshotOperator.getDeletedFiles(oldSnapshot,newSnapshot);
            Set<String> oldDirectory = SnapshotOperator.getOldFolders(oldSnapshot,newSnapshot);
            Set<String> newDirectory = SnapshotOperator.getNewFolders(oldSnapshot,newSnapshot);
            Set<String> deletedDirectory = SnapshotOperator.getDeleteFolder(oldSnapshot,newSnapshot);

            addMapToListView(odlFileMap,fillListView,Suffix.OLD);
            addMapToListView(changedFileMap,fillListView,Suffix.CHG);
            addMapToListView(newFileMap,fillListView,Suffix.NEW);
            addMapToListView(deleteFileMap,fillListView,Suffix.DEL);

            addSetToListView(oldDirectory,directoryListView,Suffix.OLD);
            addSetToListView(newDirectory,directoryListView,Suffix.NEW);
            addSetToListView(deletedDirectory,deleteDirectoryListView,Suffix.DEL);

            deletedDirectory.forEach(s -> {
                try {
                    SnapshotFileTreeMaker.deleteFileTree(targetCurrentPath.resolve(s));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });

        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
    public void initDate(Path sourceRoot, Path targetCurrentPath, Path currentPath){
        this.sourceRoot = sourceRoot;
        this.targetCurrentPath = targetCurrentPath;
        this.currentPath = currentPath;

        previousButton.setDisable(currentPath.equals(sourceRoot));

        initListViews(currentPath,targetCurrentPath);
        pathLabel.setText(currentPath.toString());
    }


    @FXML
    private void goToNextDirectory(ActionEvent actionEvent) throws IOException{
        try {
            SnapshotOperator.writeSnapshotIntoDirectory(newSnapshot, targetCurrentPath);
        }catch (IOException e){
            System.out.println("Nie udalo sie utworzyc nowe snapshota");
            System.out.println(e.getMessage());
        }


        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/MainView.fxml"));
        Parent root = fxmlLoader.load();

        String directoryName = directoryListView.getSelectionModel().getSelectedItem()
                .replace("?" + Suffix.OLD.name() + "?","")
                .replace("?" + Suffix.NEW.name() + "?","");

        MainController mainController = fxmlLoader.getController();
        mainController.initDate(
                sourceRoot,
                targetCurrentPath.resolve(directoryName),
                currentPath.resolve(directoryName)
        );

        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void goToPreviousDirectory(ActionEvent actionEvent) throws IOException{
        try {
            if(!Files.exists(targetCurrentPath))
                Files.createDirectories(targetCurrentPath);
            SnapshotOperator.writeSnapshotIntoDirectory(newSnapshot, targetCurrentPath);
        }catch (IOException e){
            System.out.println("Nie udalo sie utworzyc nowe snapshota");
            System.out.println(e.getMessage());
        }

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/MainView.fxml"));
        Parent root = fxmlLoader.load();

        MainController mainController = fxmlLoader.getController();
        mainController.initDate(
                sourceRoot,
                targetCurrentPath.resolve("..").normalize(),
                currentPath.resolve("..").normalize()
        );

        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void refresh(ActionEvent actionEvent) throws IOException{
        if(Files.exists(targetCurrentPath))
            SnapshotOperator.writeSnapshotIntoDirectory(newSnapshot, targetCurrentPath);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("views/MainView.fxml"));
        Parent root = fxmlLoader.load();

        MainController mainController = fxmlLoader.getController();
        mainController.initDate(sourceRoot, targetCurrentPath, currentPath);

        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        directoryListView.getSelectionModel().selectedItemProperty().addListener(
                (observableValue, tSandMandS, t1) -> goNextDirectory.setDisable(false)
        );
    }


}
