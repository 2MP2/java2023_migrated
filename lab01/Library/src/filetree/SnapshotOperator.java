package filetree;

import md5.MD5Operator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SnapshotOperator {

    SnapshotOperator(){
    }

    private static Map<String,String> makeSnapshotMap(Path dir) throws IOException {
        try (Stream<Path> stream = Files.list(dir)){
            return  stream
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toMap(path ->
                        path.getFileName().toString(),path -> {
                        try {
                            return MD5Operator.createMD5Shortcut(path);
                        } catch ( IOException e) {
                            throw new RuntimeException(e); //byc moze do usuniecia
                        }
                    }));
        }
    }

    private static Set<String> makeSnapshotSet(Path dir) throws IOException {
        try (Stream<Path> stream = Files.list(dir)){
            return  stream
                    .filter(Files::isDirectory)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toSet());
        }
    }

    public static Snapshot createSnapshot(Path path) throws IOException {
        return new Snapshot(makeSnapshotMap(path),makeSnapshotSet(path));
    }

    public static void writeSnapshotIntoDirectory(Snapshot snapshot, Path path) throws IOException {
        path = path.resolve("snaphot.dat");
        try (ObjectOutputStream snapshotFile = new ObjectOutputStream(new BufferedOutputStream(Files.newOutputStream(path)))) {
            snapshotFile.writeObject(snapshot);
            snapshotFile.flush();
        }
    }

    public static Snapshot readSnapshotFromDirectory(Path path) throws IOException, ClassNotFoundException {
        path = path.resolve("snaphot.dat");
        try (ObjectInputStream snapshotFile = new ObjectInputStream(new BufferedInputStream(Files.newInputStream(path)))){
            return (Snapshot) snapshotFile.readObject();
        }
    }

    public static Map<String,String> getNotChangedFiles(Snapshot oldSnapshot, Snapshot newSnapshot){

        return  oldSnapshot.getMd5Map().entrySet()
                .stream()
                .filter(stringStringEntry -> newSnapshot.getMd5Map().entrySet().contains(stringStringEntry)) //name of the file and file content need to be the same
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

    public static Map<String,String> getChangedFiles(Snapshot oldSnapshot, Snapshot newSnapshot){

        return  oldSnapshot.getMd5Map().entrySet()
                .stream()
                .filter(stringStringEntry -> newSnapshot.getMd5Map().containsKey(stringStringEntry.getKey())) //name of the file need to be the same
                .filter(stringStringEntry -> !newSnapshot.getMd5Map().containsValue(stringStringEntry.getValue())) //file content must be different
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

    public static Map<String,String> getNewFiles(Snapshot oldSnapshot, Snapshot newSnapshot){

        return newSnapshot.getMd5Map().entrySet()
                .stream()
                .filter(stringStringEntry -> !oldSnapshot.getMd5Map().containsKey(stringStringEntry.getKey())) //name of the file is only in newSnapshot
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

    public static Map<String,String> getDeletedFiles(Snapshot oldSnapshot, Snapshot newSnapshot){

        return oldSnapshot.getMd5Map().entrySet()
                .stream()
                .filter(stringStringEntry -> !newSnapshot.getMd5Map().containsKey(stringStringEntry.getKey())) //name of the file is only in oldSnapshot
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

    public static Set<String> getNewFolders(Snapshot oldSnapshot, Snapshot newSnapshot){
        return newSnapshot.getFolderSet()
                .stream()
                .filter(s -> !oldSnapshot.getFolderSet().contains(s)) //folder is only in newSnapshot
                .collect(Collectors.toSet());
    }

    public static Set<String> getOldFolders(Snapshot oldSnapshot, Snapshot newSnapshot){
        return newSnapshot.getFolderSet()
                .stream()
                .filter(s -> oldSnapshot.getFolderSet().contains(s)) //folder is only in newSnapshot and oldSnapshot
                .collect(Collectors.toSet());
    }

    public static Set<String> getDeleteFolder(Snapshot oldSnapshot, Snapshot newSnapshot){
        return oldSnapshot.getFolderSet()
                .stream()
                .filter(s -> !newSnapshot.getFolderSet().contains(s)) //folder is only in oldSnapshot
                .collect(Collectors.toSet());
    }




}