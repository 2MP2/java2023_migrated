package filetree;


import java.io.Serializable;
import java.util.*;

public class Snapshot implements Serializable {
    private final long serialVersion = 1L;
    private final Map <String,String> md5Map;
    private final Set<String> folderSet;


    public Snapshot(Map<String, String> md5Map, Set<String> set) {
        this.md5Map = md5Map;
        this.folderSet = set;
    }

    public Map<String, String> getMd5Map() {
        return Collections.unmodifiableMap(md5Map);
    }

    public Set<String> getFolderSet() {
        return Collections.unmodifiableSet(folderSet);
    }
}
