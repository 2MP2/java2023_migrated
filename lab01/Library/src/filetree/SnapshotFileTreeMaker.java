package filetree;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class SnapshotFileTreeMaker extends SimpleFileVisitor<Path> {

    public static void creatSnapshotFileTree(Path sourceRoot, Path targetRoot)throws IOException{
        Files.walkFileTree(sourceRoot,new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)  {
                Path relativizedPath = sourceRoot.relativize(dir);
                Path copyDir = targetRoot.resolve(relativizedPath);
                try {
                    if (!Files.exists(copyDir)) {
                        Files.copy(dir, copyDir);
                        Snapshot snapshot = SnapshotOperator.createSnapshot(dir);
                        SnapshotOperator.writeSnapshotIntoDirectory(snapshot,copyDir);
                    }
                }catch (IOException e){
                    System.out.println(e.getMessage());
                    return  FileVisitResult.SKIP_SUBTREE;
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc){
                System.out.println(exc.getMessage());
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public static void deleteFileTree(Path pathToDelete) throws IOException {

        Files.walkFileTree(pathToDelete, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }
                });
    }


}
