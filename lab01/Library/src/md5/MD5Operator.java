package md5;


import org.apache.commons.codec.digest.DigestUtils;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MD5Operator {
    static public String createMD5Shortcut (Path path) throws IOException {
//        MessageDigest md = MessageDigest.getInstance("md5");
//        md.update(Files.readAllBytes(path));
//        byte[] digest = md.digest();
//        return DatatypeConverter
//                .printHexBinary(digest).toUpperCase();
        return DigestUtils
                .md5Hex(Files.readAllBytes(path)).toUpperCase();
    }

}
