package pl.edu.pwr.pastuszek.isp;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.edu.pwr.pastuszek.isp.model.AccountReceivable;
import pl.edu.pwr.pastuszek.isp.model.Installation;
import pl.edu.pwr.pastuszek.isp.service.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class Automatic {
    private final AccountReceivableService accountReceivableService;
    private final InstallationService installationService;
    private LocalDate actualDate = LocalDate.now();
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public void incrementActualDate(Long days){
        actualDate = actualDate.plusDays(days);
    }

    private void sendRemainderToPay(){
        List<Installation> installations = installationService.getInstallations();
        for(Installation i: installations){
            if(Boolean.TRUE.equals(i.getIsActive())){

                AccountReceivable accountReceivable =
                        accountReceivableService.getARByIdInstallSortDateMax(i.getId());

                if(accountReceivable !=null){
                    LocalDate temDate = accountReceivable.getDateOfDeadline().plusMonths(1);
                    if(actualDate.isEqual(temDate)){
                        creatNewAccountReceivableAndAddToDB(i);
                    }
                }else{
                    creatNewAccountReceivableAndAddToDB(i);
                }

            }
        }
    }
    private AccountReceivable creatNewAccountReceivableAndAddToDB(Installation installation){
        AccountReceivable accountReceivable = AccountReceivable.builder()
                .dateOfDeadline(actualDate.plusMonths(1))
                .amountToPay(installation.getPriceList().getPrice())
                .installation(installation)
                .build();
        accountReceivableService.addAccountReceivable(accountReceivable);
        return accountReceivable;
    }

    private void sendRemainderToPayDeb(){
        List<AccountReceivable> accountReceivables = accountReceivableService.getByPaymentsIsNull();
        if(accountReceivables != null){
            for(AccountReceivable a: accountReceivables)
                if(actualDate.isAfter(a.getDateOfDeadline())){
                    try {
                        addToFile(
                                "Zapłać za " + a + "\n"
                        );
                    }catch (IOException e){}
                }
        }

    }

    private void addToFile(String content) throws IOException {
        Path filePath = Path.of("src/main/resources/file.txt");
        if (!Files.exists(filePath)) {
            Files.createDirectories(filePath.getParent());
            Files.createFile(filePath);
        }
        Files.writeString(filePath, content, StandardOpenOption.APPEND);
    }

    public void start(){
        executorService.scheduleAtFixedRate(
                ()->{
                    sendRemainderToPay();
                    sendRemainderToPayDeb();
                    incrementActualDate(1L);
                },
                1, 1, TimeUnit.SECONDS);
    }

    public void stop(){
        executorService.shutdown();
    }

}
