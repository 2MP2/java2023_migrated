package pl.edu.pwr.pastuszek.isp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.Installation;

import java.util.List;

@RequestMapping("/default")
public interface InstallationOperations {
    @GetMapping
    @Operation(summary = "Get all of the Installations",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved list of installations"),
                    @ApiResponse(responseCode = "401", description = "You are not authorized to view the resource"),
                    @ApiResponse(responseCode = "403", description = "Accessing the resource you were trying to reach is forbidden"),
                    @ApiResponse(responseCode = "404", description = "The resource you were trying to reach is not found")
            })
    List<Installation> getInstallations();

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find and get Installation by Id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved installation by id"),
                    @ApiResponse(responseCode = "404", description = "The resource you were trying to reach is not found")
            })
    Installation getInstallationById(@PathVariable("id") Long id);

    @PostMapping
    @Operation(summary = "Create new Installation",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Successfully created new installation"),
                    @ApiResponse(responseCode = "409", description = "The resource you were trying to create already exists")
            })
    void newInstallation(@RequestBody Installation installation);

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete Installation by Id",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Successfully deleted installation by id"),
                    @ApiResponse(responseCode = "404", description = "The resource you were trying to delete is not found")
            })
    void deleteInstallation(@PathVariable("id") Long id);

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update Installation by Id",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Successfully updated installation by id"),
                    @ApiResponse(responseCode = "400", description = "Invalid request parameters"),
                    @ApiResponse(responseCode = "404", description = "The resource you were trying to update is not found")
            })
    void updateInstallation(@PathVariable("id") @Parameter(description = "The ID of the installation to update") Long id,
                            @RequestParam(required = false) @Parameter(description = "The new address for the installation") String address,
                            @RequestParam(required = false) @Parameter(description = "The new router number for the installation") Long routerNumber,
                            @RequestParam(required = false) @Parameter(description = "Whether the installation is active or not") Boolean isActive,
                            @RequestParam(required = false) @Parameter(description = "The ID of the price list for the installation") Long priceListId);
}
