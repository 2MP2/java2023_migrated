package pl.edu.pwr.pastuszek.isp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.Client;

import java.util.List;

@RequestMapping("/default")
public interface ClientOperations {

    @GetMapping
    @Operation(summary = "Get all clients",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved"),
                    @ApiResponse(responseCode = "404", description = "Not found - No clients found")
            })
    List<Client> getClients();

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find and get client by ID",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved"),
                    @ApiResponse(responseCode = "404", description = "Not found - The client was not found")
            })
    Client getClientById(@PathVariable("id") Long id);

    @PostMapping
    @Operation(summary = "Create a new client",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Successfully created"),
                    @ApiResponse(responseCode = "400", description = "Bad request - Invalid input")
            })
    void newClient(@RequestBody Client client);

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete client by ID",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Successfully deleted"),
                    @ApiResponse(responseCode = "404", description = "Not found - The client was not found")
            })
    void deleteClient(@PathVariable("id") Long id);

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update client by ID",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully updated"),
                    @ApiResponse(responseCode = "404", description = "Not found - The client was not found")
            })
    void updateClient(@PathVariable("id") Long id,
                      @RequestParam(required = false) @Parameter(description = "New name of the client") String name,
                      @RequestParam(required = false) @Parameter(description = "New surname of the client") String surname,
                      @RequestParam(required = false) @Parameter(description = "New phone number of the client") String phoneNumber);

}
