package pl.edu.pwr.pastuszek.isp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.Client;
import pl.edu.pwr.pastuszek.isp.service.ClientService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/client")
public class ClientController implements ClientOperations {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    public List<Client> getClients(){
        return clientService.getClients();
    }

    @Override
    public Client getClientById(Long id){
        return clientService.getClientById(id)
                .orElseThrow(()-> new IllegalStateException(
                        "client with id: " + id + " dose not exists"
                ));
    }

    @Override
    public void newClient(Client client){
        clientService.addClient(client);
    }

    @Override
    public void deleteClient(Long id){
        clientService.deleteClient(id);
    }

    @Override
    public void updateClient(Long id, String name, String surname, String phoneNumber){
        clientService.updateClient(id, name, surname, phoneNumber);
    }
}
