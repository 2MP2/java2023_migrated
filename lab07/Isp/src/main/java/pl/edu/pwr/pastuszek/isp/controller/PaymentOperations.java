package pl.edu.pwr.pastuszek.isp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.Payment;

import java.time.LocalDate;
import java.util.List;



@RequestMapping("/default")
public interface PaymentOperations {

    @GetMapping
    @Operation(summary = "Get all Payments", responses = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved list of payments",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Payment.class))),
            @ApiResponse(responseCode = "404", description = "The resource you were trying to reach is not found")
    })
    List<Payment> getPayments();

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find and get Payment by Id", responses = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved payment",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Payment.class))),
            @ApiResponse(responseCode = "404", description = "Payment not found")
    })
    Payment getPaymentById(@PathVariable("id") Long id);

    @PostMapping
    @Operation(summary = "Add a new Payment", responses = {
            @ApiResponse(responseCode = "201", description = "Payment created successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid input parameters")
    })
    void newPayment(@RequestBody Payment payment);

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete Payment by Id", responses = {
            @ApiResponse(responseCode = "204", description = "Payment deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Payment not found")
    })
    void deletePayment(@PathVariable("id") Long id);

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update Payment by Id", responses = {
            @ApiResponse(responseCode = "200", description = "Payment updated successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid input parameters"),
            @ApiResponse(responseCode = "404", description = "Payment not found")
    })
    void updatePayment(@PathVariable("id") Long id,
                       @RequestParam(required = false) @Schema(description = "Date of payment") LocalDate dateOfPayment,
                       @RequestParam(required = false) @Schema(description = "Amount paid") Double amountPaid);

}
