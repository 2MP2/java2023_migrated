package pl.edu.pwr.pastuszek.isp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "installation")
public class Installation {
    @Id
    @SequenceGenerator(
            name = "installation_sequence",
            sequenceName = "installation_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "installation_sequence"
    )
    private Long id;
    private String address;
    @Column(unique = true)
    private Long routerNumber;
    @Column(nullable = false)
    private Boolean isActive;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "price_list_id")
    private PriceList priceList;
    @OneToMany(mappedBy = "installation")
    private List<AccountReceivable> accountReceivables;

    @Override
    public String toString() {
        return this.id + " " + this.address + " " + this.routerNumber + " " +
                (Boolean.TRUE.equals(this.isActive) ? "AKTYWNY" : "NIE AKTYWNY") + " " + this.client.getId() + " " + this.priceList.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Installation that = (Installation) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
