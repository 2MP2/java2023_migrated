package pl.edu.pwr.pastuszek.isp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.AccountReceivable;
import pl.edu.pwr.pastuszek.isp.service.AccountReceivableService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "/api/account-receivable")
public class AccountReceivableController implements AccountReceivableOperations {
    private final AccountReceivableService accountReceivableService;

    @Autowired
    public AccountReceivableController(AccountReceivableService accountReceivableService) {
        this.accountReceivableService = accountReceivableService;
    }

    @Override
    public List<AccountReceivable> getAccountReceivables(){
        return accountReceivableService.getAccountReceivables();
    }

    @Override
    public AccountReceivable getAccountReceivableById(Long id){
        return accountReceivableService.getAccountReceivableById(id)
                .orElseThrow(()-> new IllegalStateException(
                        "account receivable with id: " + id + " dose not exists"
                ));
    }

    @Override
    public void newAccountReceivable(AccountReceivable accountReceivable){
        accountReceivableService.addAccountReceivable(accountReceivable);
    }

    @Override
    public void deleteAccountReceivable(Long id){
        accountReceivableService.deleteAccountReceivable(id);
    }

    @Override
    public void updateAccountReceivable(Long id, LocalDate dateOfDeadline, Double amountToPay){
        accountReceivableService.updateAccountReceivable(id, dateOfDeadline, amountToPay);
    }
}
