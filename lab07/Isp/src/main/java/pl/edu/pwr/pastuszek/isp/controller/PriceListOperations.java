package pl.edu.pwr.pastuszek.isp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.PriceList;
import pl.edu.pwr.pastuszek.isp.model.ServiceType;

import java.util.List;

@RequestMapping("/default")
@Tag(name = "Price List Operations")
public interface PriceListOperations {

    @GetMapping
    @Operation(summary = "Get all price lists", responses = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(array = @ArraySchema(schema = @Schema(implementation = PriceList.class)))),
            @ApiResponse(responseCode = "404", description = "No price lists found")
    })
    List<PriceList> getPriceLists();

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find and get price list by id", responses = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = PriceList.class))),
            @ApiResponse(responseCode = "404", description = "Price list not found")
    })
    PriceList getPriceListById(@PathVariable("id") Long id);

    @PostMapping
    @Operation(summary = "Add new price list", responses = {
            @ApiResponse(responseCode = "201", description = "Price list created successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request body")
    })
    void newPriceList(@RequestBody PriceList priceList);

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete price list by id", responses = {
            @ApiResponse(responseCode = "200", description = "Price list deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Price list not found")
    })
    void deletePriceList(@PathVariable("id") Long id);

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update price list by id", responses = {
            @ApiResponse(responseCode = "200", description = "Price list updated successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request body"),
            @ApiResponse(responseCode = "404", description = "Price list not found")
    })
    void updatePriceList(@PathVariable("id") Long id,
                         @RequestParam(required = false) @Parameter(name = "name", description = "New name of the price list") String name,
                         @RequestParam(required = false) @Parameter(name = "serviceType", description = "New service type of the price list") ServiceType serviceType,
                         @RequestParam(required = false) @Parameter(name = "price", description = "New price of the price list") Double price);
}
