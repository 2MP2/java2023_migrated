package pl.edu.pwr.pastuszek.isp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.Payment;
import pl.edu.pwr.pastuszek.isp.service.PaymentService;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "/api/payment")
public class PaymentController implements PaymentOperations {

    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Override
    public List<Payment> getPayments(){
        return paymentService.getPayments();
    }

    @Override
    public Payment getPaymentById(Long id){
        return paymentService.getPaymentById(id)
                .orElseThrow(()-> new IllegalStateException(
                        "payment with id: " + id + " dose not exists"
                ));
    }

    @Override
    public void newPayment(Payment payment){
        paymentService.addPayment(payment);
    }

    @Override
    public void deletePayment(Long id){
        paymentService.deletePayment(id);
    }

    @Override
    public void updatePayment(Long id, LocalDate dateOfPayment, Double amountPaid){
        paymentService.updatePayment(id, dateOfPayment, amountPaid);
    }
}
