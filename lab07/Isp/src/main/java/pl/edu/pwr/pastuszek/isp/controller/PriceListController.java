package pl.edu.pwr.pastuszek.isp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.PriceList;
import pl.edu.pwr.pastuszek.isp.model.ServiceType;
import pl.edu.pwr.pastuszek.isp.service.PriceListService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/price-list")
public class PriceListController implements PriceListOperations {

    private final PriceListService priceListService;

    @Autowired
    public PriceListController(PriceListService priceListService) {
        this.priceListService = priceListService;
    }

    @Override
    public List<PriceList> getPriceLists(){
        return priceListService.getPriceLists();
    }

    @Override
    public PriceList getPriceListById(Long id){
        return priceListService.getPriceListById(id)
                .orElseThrow(()-> new IllegalStateException(
                        "price list with id: " + id + " dose not exists"
                ));
    }

    @Override
    public void newPriceList(PriceList priceList){
        priceListService.addPriceList(priceList);
    }

    @Override
    public void deletePriceList(Long id){
        priceListService.deletePriceList(id);
    }

    @Override
    public void updatePriceList(Long id, String name, ServiceType serviceType, Double price){
        priceListService.updatePriceList(id, name, serviceType, price);
    }
}
