package pl.edu.pwr.pastuszek.isp.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.pastuszek.isp.model.PriceList;
import pl.edu.pwr.pastuszek.isp.model.ServiceType;
import pl.edu.pwr.pastuszek.isp.repository.PriceListRepository;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class PriceListService {
    private final PriceListRepository priceListRepository;

    @Autowired
    public PriceListService(PriceListRepository priceListRepository) {
        this.priceListRepository = priceListRepository;
    }

    public List<PriceList> getPriceLists(){
        return priceListRepository.findAll();
    }

    public Optional<PriceList> getPriceListById(Long id){
        return priceListRepository.findById(id);
    }
    public void addPriceList(PriceList priceList){
        priceListRepository.save(priceList);
    }

    @Transactional
    public void updatePriceList(Long id, String name, ServiceType serviceType, Double price){
        PriceList priceList = priceListRepository.findById(id)
                .orElseThrow(()-> new IllegalStateException(
                   "priceList with id: " + id + " dose not exists"
                ));

        if(name !=null && name.length()>0)
            priceList.setName(name);
        if(serviceType!=null)
            priceList.setServiceType(serviceType);
        if(price !=null && price > 0d)
            priceList.setPrice(price);
    }

    public void deletePriceList(Long id){
        boolean exists = priceListRepository.existsById(id);
        if(!exists){
            throw new IllegalStateException("priceList with id: " + id + " dose not exists");
        }
        priceListRepository.deleteById(id);
    }
}
