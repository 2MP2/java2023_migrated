package pl.edu.pwr.pastuszek.isp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwr.pastuszek.isp.model.PriceList;
@Repository
public interface PriceListRepository extends JpaRepository<PriceList, Long> {
}
