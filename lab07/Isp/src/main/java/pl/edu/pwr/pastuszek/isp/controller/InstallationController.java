package pl.edu.pwr.pastuszek.isp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.Installation;
import pl.edu.pwr.pastuszek.isp.service.InstallationService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/installation")
public class InstallationController implements InstallationOperations {
    private final InstallationService installationService;

    @Autowired
    public InstallationController(InstallationService installationService) {
        this.installationService = installationService;
    }

    @Override
    public List<Installation> getInstallations(){
        return installationService.getInstallations();
    }

    @Override
    public Installation getInstallationById(Long id){
        return installationService.getInstallationById(id)
                .orElseThrow(()-> new IllegalStateException(
                        "installation with id: " + id + " dose not exists"
                ));
    }

    @Override
    public void newInstallation(Installation installation){
        installationService.addInstallation(installation);
    }

    @Override
    public void deleteInstallation(Long id){
        installationService.deleteInstallation(id);
    }

    @Override
    public void updateInstallation(Long id, String address, Long routerNumber, Boolean isActive, Long priceListId){
        installationService.updateInstallation(id, address, routerNumber, isActive, priceListId);
    }
}
