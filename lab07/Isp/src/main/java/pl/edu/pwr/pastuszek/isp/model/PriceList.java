package pl.edu.pwr.pastuszek.isp.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "price_list")
public class PriceList {
    @Id
    @SequenceGenerator(
            name = "price_list_sequence",
            sequenceName = "price_list_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "price_list_sequence"
    )
    private Long id;
    @Column(unique = true)
    private String name;
    @Enumerated(value = EnumType.STRING)
    private ServiceType serviceType;
    @Column(nullable = false)
    private Double price;
    @OneToMany(mappedBy = "priceList")
    private List<Installation> installations;

    @Override
    public String toString() {
        return this.id + " " + this.name + " " + this.serviceType + " " + String.format("%f.2",this.price);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceList priceList = (PriceList) o;
        return Objects.equals(id, priceList.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
