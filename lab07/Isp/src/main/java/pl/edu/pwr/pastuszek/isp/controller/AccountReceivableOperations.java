package pl.edu.pwr.pastuszek.isp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.pastuszek.isp.model.AccountReceivable;

import java.time.LocalDate;
import java.util.List;

@RequestMapping("/default")
public interface AccountReceivableOperations {

    @GetMapping
    @Operation(summary = "Get all of the AccountReceivables",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved")
            })
    List<AccountReceivable> getAccountReceivables();

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find and get AccountReceivable by Id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved"),
                    @ApiResponse(responseCode = "404", description = "Not found - The AccountReceivable was not found")
            })
    AccountReceivable getAccountReceivableById(@PathVariable("id") Long id);

    @PostMapping
    @Operation(summary = "Create a new AccountReceivable",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Successfully created")
            })
    void newAccountReceivable(@RequestBody AccountReceivable accountReceivable);

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete AccountReceivable by Id",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Successfully deleted"),
                    @ApiResponse(responseCode = "404", description = "Not found - The AccountReceivable was not found")
            })
    void deleteAccountReceivable(@PathVariable("id") Long id);

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update AccountReceivable by Id",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Successfully updated"),
                    @ApiResponse(responseCode = "404", description = "Not found - The AccountReceivable was not found")
            })
    void updateAccountReceivable(@PathVariable("id") Long id,
                                 @RequestParam(required = false) @Parameter(description = "New deadline date") LocalDate dateOfDeadline,
                                 @RequestParam(required = false) @Parameter(description = "New amount to pay") Double amountToPay);
}
