package pl.edu.pwr.pastuszek.aplicationjca;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.edu.pwr.pastuszek.encryption.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.ResourceBundle;

public class StartController implements Initializable {
    @FXML private TextField keyStoreTextField, aliasTextField, inFileTextField, outFolderTextField, secretKeyCrTextField;
    @FXML private PasswordField passwordField;
    @FXML private TextArea logTextArea;
    @FXML private AnchorPane anchorPane;
    @FXML private CheckBox confirmCheckBox;

    private PrivateKey privateKey;
    private PublicKey publicKey;
    private CustomKeyStore customKeyStore;
    private CustomKeyGenerator customKeyGenerator;
    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Variable variable = new Variable();
        this.customKeyStore = CustomKeyStore.getCustomKeyStore();
        this.customKeyGenerator = new CustomKeyGenerator();
        variable.communicat();
    }

    @FXML
    private void findKeyStoreTextField(){
        File file = getFile();

        try {
            keyStoreTextField.setText(file.getAbsolutePath());
        }catch (NullPointerException e){
            logTextArea.setText(e.getMessage() + "\n");
        }
    }
    @FXML
    private void findInFileTextField(){
        File file = getFile();

        try {
            inFileTextField.setText(file.getAbsolutePath());
        }catch (NullPointerException e){
            logTextArea.appendText(e.getMessage() + "\n");
        }
    }
    @FXML
    private void findOutFolderTextField(){
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        File file = directoryChooser.showDialog(stage);

        if(file !=null && file.isDirectory()){
            outFolderTextField.setText(file.getAbsolutePath());
        }
    }

    @FXML
    private void findSecretKeyCrTextField(){
        File file = getFile();
        try {
            secretKeyCrTextField.setText(file.getAbsolutePath());
        }catch (NullPointerException e){
            logTextArea.appendText(e.getMessage() + "\n");
        }
    }

    @FXML
    private void loadKeys(){
        if(keyStoreTextField.getText().isBlank() || aliasTextField.getText().isBlank() || passwordField.getText().isBlank()){
            logTextArea.appendText("Brak wymaganych pól" + "\n");
            return;
        }


        try {
            X509Certificate x509Certificate = customKeyStore.getX509CertificateFromKeyStore(new File(keyStoreTextField.getText()), passwordField.getText().toCharArray(), aliasTextField.getText());
            publicKey = x509Certificate.getPublicKey();
            privateKey = customKeyStore.getPrivateKeyFromCertificate(new File(keyStoreTextField.getText()), passwordField.getText().toCharArray(), aliasTextField.getText(), passwordField.getText().toCharArray());

            logTextArea.appendText("SUKCES!!! Załadowano klucze." + "\n");
        } catch (IOException | GeneralSecurityException e) {
            logTextArea.appendText(e.getMessage() + "\n");
        }
        passwordField.clear();
    }

    @FXML
    private void genCertificate(){
        if(keyStoreTextField.getText().isBlank() || aliasTextField.getText().isBlank() || passwordField.getText().isBlank()){
            logTextArea.appendText("Brak wymaganych pól" + "\n");
            return;
        }

        try {
            customKeyGenerator.customCertificateGen(aliasTextField.getText(), new File(keyStoreTextField.getText()), passwordField.getText());
            logTextArea.appendText("SUKCES!!! Wygenerowano certyfikat." + "\n");
        }catch (CustomGeneratingException e){
            logTextArea.appendText(e.getMessage() + "\n");
        }
        passwordField.clear();
    }

    @FXML
    private void deleteCertificate(){
        if(keyStoreTextField.getText().isBlank() || aliasTextField.getText().isBlank() || passwordField.getText().isBlank()){
            logTextArea.appendText("Brak wymaganych pól" + "\n");
            return;
        }

        if(!confirmCheckBox.isSelected()){
            logTextArea.appendText("Powierdź usnięcie" + "\n");
            return;
        }

        try {
            customKeyGenerator.deleteCertificate(aliasTextField.getText(), new File(keyStoreTextField.getText()), passwordField.getText());
            logTextArea.appendText("SUKCES!!! Usunięto certyfikat." + "\n");
        }catch (CustomGeneratingException e){
            logTextArea.appendText(e.getMessage() + "\n");
        }
        passwordField.clear();

    }

    @FXML
    private void encrypt(){
        if(privateKey==null || publicKey==null){
            logTextArea.appendText("Niewczytano kluczy." + "\n");
        }

        File inFile = new File(inFileTextField.getText());
        File outDir = new File(outFolderTextField.getText());

        if(!inFile.isFile() || !outDir.isDirectory()){
            logTextArea.appendText("Plik wejsciowy lub Folder wyjściowy jest zły." + "\n");
        }

        long date = new Date().getTime();
        File outFile = new File(outDir.getAbsolutePath() + System.getProperty("file.separator") + date + inFile.getName());
        String keyPath = outDir.getAbsolutePath() + System.getProperty("file.separator") + date + "KEY.txt";

        try(FileOutputStream keyFileOutputStream = new FileOutputStream(keyPath)){

            RSAEncryption rsaEncryption = new RSAEncryption();
            SymmetricEncryption symmetricEncryption = new SymmetricEncryption();
            SecretKey secretKey = customKeyGenerator.genSecretKeyASC();
            IvParameterSpec ivParameterSpec = symmetricEncryption
                    .encryptFile(ALGORITHM,secretKey,inFile,outFile);

            byte[] ivParameterSpecEnc = rsaEncryption.encrypt(ivParameterSpec.getIV(), publicKey);
            byte[] secretKeyEnc = rsaEncryption.encrypt(secretKey.getEncoded(),publicKey);

            keyFileOutputStream.write(secretKeyEnc);
            keyFileOutputStream.write(ivParameterSpecEnc);

            secretKeyCrTextField.setText(keyPath);

            logTextArea.appendText("Udało się zaszyfrować." + "\n");
        }catch (IOException | GeneralSecurityException e) {
            logTextArea.appendText(e.getMessage() + "\n");
        }
    }

    @FXML
    private void decrypt() {
        if (privateKey == null || publicKey == null) {
            logTextArea.appendText("Niewczytano kluczy." + "\n");
        }

        File inFile = new File(inFileTextField.getText());
        File outDir = new File(outFolderTextField.getText());

        if (!inFile.isFile() || !outDir.isDirectory()) {
            logTextArea.appendText("Plik wejsciowy lub Folder wyjściowy jest zły." + "\n");
        }

        if (secretKeyCrTextField.getText().isBlank()) {
            logTextArea.appendText("Niewczytano podano scierzek do czesci symetrycznej" + "\n");
        }

        try (FileInputStream keyFileInputStream = new FileInputStream(secretKeyCrTextField.getText())) {

            RSAEncryption rsaEncryption = new RSAEncryption();
            SymmetricEncryption symmetricEncryption = new SymmetricEncryption();
            SecretKey secretKey = rsaEncryption.decryptRSAtoSecretKey(keyFileInputStream.readNBytes(256), privateKey);
            IvParameterSpec ivParameterSpec = rsaEncryption.decryptRSAtoIv(keyFileInputStream.readNBytes(256), privateKey);

            File outFile = new File(outDir.getAbsolutePath() + System.getProperty("file.separator") + "DEC" + inFile.getName());

            symmetricEncryption.decryptFile(ALGORITHM,secretKey,inFile,outFile,ivParameterSpec);

            logTextArea.appendText("Udało się odszyfrować." + "\n");
        } catch (IOException | GeneralSecurityException e) {
            logTextArea.appendText(e.getMessage() + "\n");
        }
    }

    private File getFile(){
        final FileChooser fileChooser = new FileChooser();
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        return fileChooser.showOpenDialog(stage);
    }
}