module com.example.filefromcomputerreader {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.filefromcomputerreader to javafx.fxml;
    exports com.example.filefromcomputerreader;
}