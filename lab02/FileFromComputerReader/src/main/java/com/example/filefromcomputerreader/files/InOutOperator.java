package com.example.filefromcomputerreader.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InOutOperator {
    private InOutOperator(){}
    public static Set<String> getDirectories(Path dir) throws IOException {
        try (Stream<Path> stream = Files.list(dir)){
            return  stream
                    .filter(Files::isDirectory)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toSet());
        }
    }

    public static Set<String> getFiles(Path dir) throws IOException {
        try (Stream<Path> stream = Files.list(dir)){
            return  stream
                    .filter(Files::isRegularFile)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toSet());
        }
    }


}
