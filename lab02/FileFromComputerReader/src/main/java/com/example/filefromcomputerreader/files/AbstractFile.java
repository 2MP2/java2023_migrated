package com.example.filefromcomputerreader.files;

import java.nio.file.Path;

public abstract class AbstractFile implements IContentAndInfo,IToTableView {
    protected final Path path;
    protected final String typeOfFile;

    AbstractFile(Path path, String typeOfFile) {
        this.path = path;
        this.typeOfFile = typeOfFile;
    }

    public Path getPath() {
        return path;
    }

    public String getTypeOfFile() {
        return typeOfFile;
    }



}
