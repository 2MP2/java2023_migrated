package com.example.filefromcomputerreader;

import com.example.filefromcomputerreader.files.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ResourceBundle;
import java.util.WeakHashMap;

public class StartController implements Initializable{
    @FXML private TextField pathTextField;
    @FXML private Label infoLabel;
    @FXML private ListView<String> directoryListView, filleListView;
    @FXML private TableView<String[]> previewTableView;
    @FXML private TextArea infoTextArea;
    @FXML private AnchorPane anchorPane;

    private Path currentPath;
    private static final WeakHashMap<Path,AbstractFile> fileMap;

    static{
        fileMap = new WeakHashMap<>();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        directoryListView.setOnMouseClicked(mouseEvent -> {
            if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                currentPath = currentPath.resolve(directoryListView.getSelectionModel().getSelectedItem()).toAbsolutePath();
            }else if(mouseEvent.getButton().equals(MouseButton.SECONDARY)){
                currentPath = currentPath.resolve("..").toAbsolutePath();
            }
            try {
                directoryListView.getItems().clear();
                filleListView.getItems().clear();
                directoryListView.getItems().addAll(InOutOperator.getDirectories(currentPath));
                filleListView.getItems().addAll(InOutOperator.getFiles(currentPath));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        filleListView.setOnMouseClicked(mouseEvent -> {
            Path filePath = currentPath.resolve(filleListView.getSelectionModel().getSelectedItem());
            if(filePath.toString().contains(".csv")){
                try {
                    if(fileMap.put(filePath, new CsvFile(filePath)) == null)
                        infoLabel.setText("PAMIĘCI");
                    else
                        infoLabel.setText("MAPY");

                    showAll(fileMap.get(filePath));
                } catch (IOException e) {
                    infoLabel.setText("BŁĄD");
                }
            }
        });

    }

    @FXML
    private void findDirectory() throws IOException {
        this.currentPath = browseDirectory(pathTextField);

        if(Files.isDirectory(currentPath)){
            directoryListView.getItems().addAll(InOutOperator.getDirectories(currentPath));
            filleListView.getItems().addAll(InOutOperator.getFiles(currentPath));
        }
    }

    private Path browseDirectory(TextField textField){
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        File file = directoryChooser.showDialog(stage);

        if(file !=null && file.isDirectory()){
            textField.setText(file.getAbsolutePath());
            return file.toPath();
        }else
            return null;
    }

    private void showFile(IToTableView view){
        previewTableView.setItems(view.generateData());
        previewTableView.getColumns().setAll(view.createColumns());
    }
    private void showInfoFile(IContentAndInfo info){
        infoTextArea.setText(info.getInfo());
    }
    private void showAll(AbstractFile abstractFile){
        showFile(abstractFile);
        showInfoFile(abstractFile);
    }



}