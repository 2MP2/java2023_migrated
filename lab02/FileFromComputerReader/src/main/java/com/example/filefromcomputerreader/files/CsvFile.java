package com.example.filefromcomputerreader.files;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class CsvFile extends AbstractFile{
    private final String[][] tab;
    private final String[] headers;
    private final int N_COLS,N_ROWS;

    public CsvFile(Path path) throws IOException {
        super(path,".csv");
        String[] rows = Files.readAllLines(path).toArray(new String[0]);
        headers = rows[0].split(";");
        this.N_ROWS = rows.length -1;
        this.N_COLS = headers.length;

        if(N_COLS > 0 && N_ROWS > 0){
            tab = new String[N_ROWS][N_COLS];
            for (int i = 0; i<N_ROWS; i++)
                tab[i]= rows[i+1].split(";");
        }else
            tab = null;
    }

    @Override
    public byte[] getContent() {
        return Arrays.toString(tab).getBytes();
    }

    @Override
    public String getInfo() { // averages of columns

        Double[] average = new Double[headers.length];
        Arrays.fill(average,0d);

        for (int i = 0; i < N_COLS ; i++){
            for (int j = 0; j < N_ROWS; j++){
                try {
                    average[i]+=Double.parseDouble(tab[j][i]);
                }catch (NumberFormatException e){
                    average[i]=null;
                    break;
                }
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i<average.length; i++){
            if(average[i]!=null)
                stringBuilder.append("Średnia z kolumny ").append(headers[i]).append(": ").append(Math.round(average[i]/N_ROWS * 100.00)/100.00).append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public ObservableList<String[]> generateData() {
        return FXCollections.observableArrayList(tab);
    }
    @Override
    public List<TableColumn<String[], String>> createColumns() {
        return IntStream.range(0, N_COLS)
                .mapToObj(this::createColumn)
                .toList();
    }

    private TableColumn<String[], String> createColumn(int c) {
        TableColumn<String[], String> col = new TableColumn<>(headers[c]);
        col.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()[c]));

        return col;
    }



}
