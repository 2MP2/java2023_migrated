package com.example.filefromcomputerreader.files;

public interface IContentAndInfo {
    byte[] getContent();
    String getInfo();
}
