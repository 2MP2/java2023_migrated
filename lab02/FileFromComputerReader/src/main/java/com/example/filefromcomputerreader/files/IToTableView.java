package com.example.filefromcomputerreader.files;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;

import java.util.List;

public interface IToTableView {
    ObservableList<String[]> generateData();
     List<TableColumn<String[], String>> createColumns();

}
