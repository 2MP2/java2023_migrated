package pl.edu.pwr.pastuszek.convolution;

import java.util.Arrays;

public class ConvolutionCounter {
    public int[][] convolution(int[][] matrix1, int[][] matrix2) {
        int m1Rows = matrix1.length;
        int m1Cols = matrix1[0].length;
        int m2Rows = matrix2.length;
        int m2Cols = matrix2[0].length;
        int resultRows = m1Rows + m2Rows - 1;
        int resultCols = m1Cols + m2Cols - 1;

        // Utwórz nową macierz wynikową i wypełnij ją zerami
        int[][] result = new int[resultRows][resultCols];

        // Przeprowadź splot
        for (int i = 0; i < resultRows; i++) {
            for (int j = 0; j < resultCols; j++) {
                for (int k = Math.max(0, i - m2Rows + 1); k < Math.min(m1Rows, i + 1); k++) {
                    for (int l = Math.max(0, j - m2Cols + 1); l < Math.min(m1Cols, j + 1); l++) {
                        result[i][j] += matrix1[k][l] * matrix2[i - k][j - l];
                    }
                }
            }
        }

        return result;
    }

}
