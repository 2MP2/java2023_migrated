package pl.edu.pwr.pastuszek;

import pl.edu.pwr.pastuszek.convolution.ConvolutionCounter;
import pl.edu.pwr.pastuszek.convolution.ConvolutionJNICounter;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {


        var convolutionCounter = new ConvolutionCounter();
        var convolutionJNICounter = new ConvolutionJNICounter();

//        int[][] matrix1 = RandMatrixGen.genMatrix(4, 4, 7, 9);
//        int[][] matrix2 = RandMatrixGen.genMatrix(5, 5, 4, 8);

        int[][] matrix1 = {
                {1, 4, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] matrix2 = {
                {1, 1},
                {1, 1}
        };


        System.out.println(Arrays.deepToString(matrix1));
        System.out.println(Arrays.deepToString(matrix2));


        System.out.println("JNI");
        System.out.println(Arrays.deepToString(convolutionJNICounter.convolution(
                matrix1,
                matrix2
        )));
        System.out.println("Normal");
        System.out.println(Arrays.deepToString(convolutionCounter.convolution(
                matrix1,
                matrix2
        )));
    }
}