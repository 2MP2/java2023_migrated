package pl.edu.pwr.pastuszek.convolution;

import java.util.Random;

public class RandMatrixGen {
    private static final Random random;
    private RandMatrixGen(){}

    static {
        random = new Random();
    }

    public static int[][] genMatrix(int r, int c, int min, int max){
        if(r<1 || c<1 || min > max)
            throw new IllegalArgumentException("Error");

        int range = max - min + 1;

        int[][] matrix = new int[r][c];
        for (int i = 0; i < r; i++){
            for (int j = 0; j < c; j++ ){
                matrix[i][j]= min + random.nextInt(range);
            }
        }

        return matrix;
    }
}
