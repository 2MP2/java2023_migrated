#include <jni.h>
#include <iostream>
#include <vector>
//#include "pl_edu_pwr_pastuszek_convolution_ConvolutionJNICounter.h"


extern "C" JNIEXPORT jobjectArray JNICALL Java_pl_edu_pwr_pastuszek_convolution_ConvolutionJNICounter_convolution(JNIEnv* env, jobject obj, jobjectArray arr1, jobjectArray arr2) {
    // Pobierz wielkości macierzy
    jint m1Rows = env->GetArrayLength(arr1);
    jint m2Rows = env->GetArrayLength(arr2);
    jint m1Cols = env->GetArrayLength((jintArray) env->GetObjectArrayElement(arr1, 0));
    jint m2Cols = env->GetArrayLength((jintArray) env->GetObjectArrayElement(arr2, 0));

    // Utwórz nową macierz wynikową i wypełnij ją zerami
    std::vector<std::vector<jint>> result(m1Rows + m2Rows - 1, std::vector<jint>(m1Cols + m2Cols - 1, 0));

    // Przeprowadź splot
    for (jint i = 0; i < m1Rows; i++) {
        jintArray row1 = (jintArray) env->GetObjectArrayElement(arr1, i);
        jint* elements1 = env->GetIntArrayElements(row1, nullptr);

        for (jint j = 0; j < m1Cols; j++) {
            jint value1 = elements1[j];
            std::vector<jint>& resultRow = result[i];

            for (jint k = 0; k < m2Rows; k++) {
                jintArray row2 = (jintArray) env->GetObjectArrayElement(arr2, k);
                jint* elements2 = env->GetIntArrayElements(row2, nullptr);
                std::vector<jint>& resultRowK = result[i + k];
                jint* resultRowKData = &(resultRowK[j]);

                for (jint l = 0; l < m2Cols; l++) {
                    resultRowKData[l] += value1 * elements2[l];
                }

                env->ReleaseIntArrayElements(row2, elements2, JNI_ABORT);
            }
        }
        env->ReleaseIntArrayElements(row1, elements1, JNI_ABORT);
    }

    // Przekonwertuj wynik na macierz Javy
    jobjectArray resultArray = env->NewObjectArray(m1Rows + m2Rows - 1, env->FindClass("[I"), nullptr);
    for (jint i = 0; i < m1Rows + m2Rows - 1; i++) {
        jintArray rowArray = env->NewIntArray(m1Cols + m2Cols - 1);
        env->SetIntArrayRegion(rowArray, 0, m1Cols + m2Cols - 1, &(result[i][0]));
        env->SetObjectArrayElement(resultArray, i, rowArray);
    }

    return resultArray;
}


