package pl.edu.pwr.pastuszek.convolution;

public class ConvolutionJNICounter {
    public native int[][] convolution(int[][] matrix1, int[][] matrix2);

    static {
        System.loadLibrary("native");
    }
}
