package pl.edu.pwr.pastuszek.convolution;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class ConvolutionTest {
    ConvolutionCounter convolutionCounter;
    ConvolutionJNICounter convolutionJNICounter;
    int[][] matrix1;
    int[][] matrix2;


    @Before
    public void setup(){
        convolutionCounter = new ConvolutionCounter();
        convolutionJNICounter = new ConvolutionJNICounter();
        matrix1 = RandMatrixGen.genMatrix(40,40,0,10);
        matrix2 = RandMatrixGen.genMatrix(40,40,-5,5);
    }

    @Test
    public void convolutionJNITestTime() {
        long startTime = System.nanoTime();
        int[][] result = convolutionJNICounter.convolution(matrix1, matrix2);
        long endTime = System.nanoTime();
        long executionTime = (endTime - startTime) / 1_000_000;
        System.out.println("Czas wykonania JNI: " + executionTime + " ms");
    }

    @Test
    public void convolutionTestTime() {
        long startTime = System.nanoTime();
        int[][] result = convolutionCounter.convolution(matrix1, matrix2);
        long endTime = System.nanoTime();
        long executionTime = (endTime - startTime) / 1_000_000;
        System.out.println("Czas wykonania: " + executionTime + " ms");
    }
}